import React, { Component } from 'react';
import { View } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import Icon1 from 'react-native-vector-icons/MaterialCommunityIcons';
import Icon2 from 'react-native-vector-icons/MaterialIcons';



//Create a dedicated class that will manage the tabBar icon
export default class TabIcon extends Component {
    render() {
      var color = this.props.focused ? '#fff' : '#666666';
      return (
        <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center',marginTop:10, alignSelf: 'center', justifyContent: 'center' }}>
          <Icon style={{ color: color }} name={this.props.iconName} size={20} />
            <Icon1 style={{ color: color,marginBottom:0 }} name={this.props.iconName1} size={26} /> 
 

        </View>
      );
    }
  };