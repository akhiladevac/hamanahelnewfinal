import React, { Component } from 'react';
import { View, Text , Image, FlatList, TouchableOpacity } from 'react-native';




export default class History extends Component {
    constructor() {
        super();

        this.state = {
            abc: ''
    
        }
    }

renderList=({item}) => {
    if (item.Trans_Date) {
        var date_value = item.Trans_Date.split('-');
        var date_Correct = date_value[2] + '-' + date_value[1] + '-' + date_value[0];
    }
    return(
        <View>
        <View style={{ alignItems: 'center', flexDirection: 'row', backgroundColor: '#fff', marginTop: 10, marginLeft: 10,marginRight: 10, borderRadius: 10, height: 70}}>
            <View style={{ width: 45, height: 45, backgroundColor: '#7BCFE8', marginLeft: 10, borderRadius: 10}}>

            </View>
           <View style={{ marginLeft: 25}} >
           
                {/* <Text style={{ fontSize: 15, fontWeight: '800' }}>{item.Credit}</Text> */}
                {/* <Text style={{ fontSize: 9, paddingLeft: 2 }}>{item.User_Narration}</Text>      */}
                <Text style={{ fontSize: 15, fontWeight: '800' }}>{item.Credit} ₹</Text>
                <Text numberOfLines={1} style={{ fontSize: 9, paddingLeft: 2, width: 250, paddingTop: 10 }}>{item.User_Narration}</Text>
           </View>
        </View>
        <View>
                {/* <Text style={{ fontSize: 9, marginLeft: 170 }}>{item.Trans_Date}</Text> */}
                <Text style={{ fontSize: 9, textAlign: 'right', paddingRight: 20, marginTop: -45 }}>{date_Correct}</Text>
           </View>
        </View>
    );
}

    render() {
        return(
                <View style= {{ backgroundColor: '#EBEBEB', marginTop: 10, borderRadius:20}}>
                    <View style={{ height: 25, marginTop: 15}}>
                      <Text style={{fontSize: 17, marginLeft: 20}}>History</Text>
                    </View>
                    <FlatList
                        showsHorizontalScrollIndicator={false}
                        style={{marginTop: 5, marginBottom: 30}}                
                        data={this.props.abc}
                        renderItem={this.renderList}
                        keyExtractor={(item,index)=>index.toString()}
                    />
                </View>
        );
    }
}
    