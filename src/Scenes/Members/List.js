import React, { Component } from 'react';
import { View, Text, Image, FlatList, TouchableOpacity, Alert, Linking } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

export default class List extends Component {

renderCall(phone) {
    if (phone) {
        return (
            <TouchableOpacity onPress={() => this.callFuntion(phone)}>
                <Icon style={{marginLeft:5}}   name="phone" size={20} color="#000" />
            </TouchableOpacity>
        )
    }
}
renderEmail(email) {
       if (email) {
        return (
            <TouchableOpacity onPress={() => this.emailFuntion(email)}>
                <Icon  name="email" size={20} color="#000" />
            </TouchableOpacity>
        )
          }
}
callFuntion(number) {
  

    if (number !== null && number.length >= 8) {
      Linking.openURL(`tel:${number}`);
    } else {
      Alert.alert('Number is Empty');
    }
}
emailFuntion(email) {
    if (email !== null ) {
        Linking.openURL('mailto:'+ email +'?subject=&body=');
                            }
     else {
      Alert.alert('Email is Empty');
    }
}
renderList=({item}) => {
    console.log('testttt');
    console.log(item);
    console.log("jygtu"+item.phone);
        if (item.photo) {
            var profPic =  { uri: this.props.baseUrl + item.photo };
        }  else {
            var profPic = require('../../img/avatar.png');
        }
    
    
    if (item.email && item.email !== '0') {
        var email = item.email;
    } else {
        var email = '';
    }
    if (item.phone && item.phone !== '0') {
        var phone = item.phone;
    } else {
        var phone = '';
    }
    if (item.Status === 'Y') {
        var color = '#fff'
    } else {
        var color = 'rgba(255,50,30,0.5)'
    }
    return(
        <View>
            <View style={{ backgroundColor: '#fff', marginTop: 10, marginLeft: 10,marginRight: 10, borderRadius: 10, height: 70 }}>
                <View style={{ alignItems: 'center', flexDirection: 'row', backgroundColor: color, height: 70, borderRadius: 10,}}>
                    <Image 
                    style={{ height: 45, width: 45, marginLeft: 10, borderRadius: 20 }}
                    source={profPic}
                    /> 
                <View style={{ marginLeft: 10, width: '50%'}} >
                
                        <Text style={{ fontSize: 17, fontWeight: '500' }}>{item.Person_Name}</Text>
                        <Text style={{ fontSize: 10, paddingLeft: 2, paddingTop: 5 }}>{item.Person_Occ}</Text>
                
                </View>
           <View style={{flexDirection:'row',marginLeft:60}}>
           {this.renderEmail(email)}

                {this.renderCall(phone)}
                </View>
                
                </View>
            </View>
        </View>
    );
}

    render() {
        return(
                <View style= {{ backgroundColor: '#EBEBEB', borderRadius:20}}>
                    <FlatList
                        showsHorizontalScrollIndicator={false}
                        style={{ marginBottom: 30}}                
                        data={this.props.abc}
                        renderItem={this.renderList}
                        keyExtractor={(item,index)=>index.toString()}
                    />
                </View>
        );
    }
}
    