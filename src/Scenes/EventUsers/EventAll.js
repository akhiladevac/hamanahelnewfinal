/* eslint-disable prettier/prettier */
/* eslint-disable no-trailing-spaces */
import React, { Component } from 'react';
import { View, Text, FlatList, TouchableOpacity, Image, TextInput,BackHandler,ActivityIndicator, NetInfo } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import AsyncStorage from '@react-native-community/async-storage';
const DATA_BASE_URL = '@base_url';


export default class EventAll extends Component {

    constructor() {
        super();

        this.state = {
            loading: false,      
            data: [],
            temp: [],  
            baseUrl: '',    
            error: null,  
            abd:[]  ,
            eventsData1:[],
            connection_Status : '',
            settimeout:false,
            loadingStatus:true


          };

        this.arrayholder = [];
    }

    componentDidMount() {
        this.checkNetwork();
        this.getBaseUrl();
      
    }
    checkNetwork=() => {
        NetInfo.isConnected.addEventListener(
          'connectionChange',
          this._handleConnectivityChange
    
      );
     
      NetInfo.isConnected.fetch().done((isConnected) => {
    
        if(isConnected == true)
        {
          this.setState({connection_Status : "Online"})
        }
        else
        {
          this.setState({connection_Status : "Offline"})
    
    
    
        }
    
      });
    
      }
      componentWillUnmount() {
    
        NetInfo.isConnected.removeEventListener(
            'connectionChange',
            this._handleConnectivityChange
     
        );
     
      }
     
      _handleConnectivityChange = (isConnected) => {
     
        if(isConnected == true)
          {
            this.setState({connection_Status : "Online"})
    
          }
          else
          {
            this.setState({connection_Status : "Offline"})
    
            
    
    
          }
      };
     
    
    async getBaseUrl() {
        try {
            let baseUrl = await AsyncStorage.getItem(DATA_BASE_URL);
            this.setState({ baseUrl: baseUrl });
            this.callApiEventsList(baseUrl);
        } catch (error) {
            console.error('Something went Wrong in Get Section' + error);
        }
      }
      callApiEventsList(baseUrl){
      this.setState({ loadingStatus: true, });
      var endpoint = 'birthday_list.php';
      var url = baseUrl + endpoint;
      // console.log(url);
      var data = new FormData();
      data.append('hashcode', '##church00@');
      fetch(url, {
      method: 'POST',
      headers: {
          Accept: 'application/json',
          'Content-Type': 'multipart/form-data',
      },
      body: data,
      })
      .then((response) => response.json())
      .then((responseJson) => {
          console.log('bithday');
          // console.log(responseJson);
          if (responseJson.status === 200) {
              this.setState({ 
                eventsData1: responseJson.response,
                  settimeout:'false',
                  loadingStatus:false
              });
          } else {
            this.setState({
              settimeout:'false',
              loadingStatus:false
            })

          }
          // console.log(responseJson);
      })
      .catch((error) => {
        console.error(error);
      });
      setTimeout(() => {
          this.setState({
              loadingStatus:false,
              settimeout:true
          })
          },45000)

  }
    
renderItem = ({ item }) => {

    // console.log(item);
    // console.log(item.Person_Name);
    return (
      <TouchableOpacity  style={{ backgroundColor: '#fff', height: 70, marginTop: 10, marginLeft: 10, marginRight: 10, borderRadius: 10 }}>
            <View style={{ flexDirection: 'row', borderRadius: 10, alignItems: 'center' }}>

                <Image 
                    style={{ width: 60, height: 60, borderRadius: 30, marginLeft: 10, marginTop: -1 }}
                    source={require('../../img/avatar.png')}
                />

                <View style={{ width: 200, height: 70, justifyContent: 'center' }}>
                    <Text style={{ paddingLeft: 15, fontSize: 16, fontWeight: '500',marginTop:15}}>{ item.Person_Name }</Text>
                  <View style={{flexDirection:'row'}}>
                    
                    <Text numberOfLines={1} style={{ paddingLeft: 15,fontSize:10, paddingTop: 8, marginBottom: 20,width:130 }}>{ item.Family_Name} House</Text>
                    <Text numberOfLines={1} style={{ paddingLeft: 25, paddingTop: 8, marginBottom: 20,fontSize:12}}>{ item.Event}</Text>

                    </View>
                    
                </View>

                <View style={{ width: 200, height: 70, justifyContent: 'center',  }}>
                </View>

            </View>
            <View style={{ height: 0.2, backgroundColor: 'grey', width: '100%'}}></View>
        </TouchableOpacity>
    );
  }

  renderContents() {

    if (this.state.loadingStatus) {
        return (
            <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center',marginTop:280}}>
                <ActivityIndicator size='large' />
            </View>
        );
    }else if(this.state.eventsData1.length==0){
      return (
        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
           <Text style={{marginTop:500}}>No Event Found </Text>
        </View>
      );

    

    } 
    else if(this.state.loadingStatus==false && this.state.settimeout == true && this.state.connection_Status=='Offline' ){

        return (
            <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center',marginTop:250}}>
                <Text style={{fontSize:18}}>You are offline</Text>
                <View style={{alignItems:'center', flexDirection: 'row', alignItems: 'center',justifyContent:'center'}}>
                <TouchableOpacity onPress={() => this.callApiEventsList(this.state.baseUrl)}>
                    <Icon style={{ marginRight:20}} name="undo" size={17} color="#7BCFE8" />
                    </TouchableOpacity>
                <Text onPress={() => this.callApiEventsList(this.state.baseUrl)} style={{fontSize:18,marginTop:20,marginRight:250}}>Retry</Text>
                </View>

            </View>
        );
    }
    else   {
        return (
              <FlatList   
                style={{ marginTop: 9,}}       
                data={this.state.eventsData1}          
                renderItem={this.renderItem}          
                keyExtractor={(item, index) => index.toString()}  
                ItemSeparatorComponent={this.renderSeparator}  
                                          
            />       
        )
    }
}


    render() {
        return (
            <View style={{ borderTopWidth: 0, borderBottomWidth: 0 }}>
            
                <View style={{ backgroundColor: '#7BCFE8'}}>
                <View style={{ flexDirection: 'row', alignItems: 'center', paddingLeft: 10, backgroundColor: '#7BCFE8', height: 80, flexDirection: 'row' }}>
                <Text style={{ fontSize: 22, fontWeight: 'bold',marginLeft:10 }}>Today's events</Text>

                </View>
                </View>
                {this.renderContents()}
                {/* <FlatList   
                style={{ marginTop: 9,}}       
                data={this.state.eventsData}          
                renderItem={this.renderItem}          
                keyExtractor={(item, index) => index.toString()}  
                ItemSeparatorComponent={this.renderSeparator}  
                                          
            />        */}
            </View>
        );
    }
}