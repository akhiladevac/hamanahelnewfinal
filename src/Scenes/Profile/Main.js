/* eslint-disable prettier/prettier */
import React, { Component } from 'react';
import { View, Text , Image,ScrollView, ActivityIndicator, TouchableOpacity,NetInfo, Alert,TextInput,BackHandler } from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import List from '../Profile/List';
import EditProfile from '../Profile/EditProfile';

import { Actions } from 'react-native-router-flux';
import Icon from 'react-native-vector-icons/FontAwesome';

import Dialog, { DialogTitle, DialogFooter, DialogButton, DialogContent } from 'react-native-popup-dialog';

const DATA_BASE_URL = '@base_url';
const LOGIN_CREDENTIALS = '@login_credentials';
export default class Profile extends Component {

    constructor() {
        super();
        this.callApiProfileUpdate = this.callApiProfileUpdate.bind(this);


        this.state = {
            response: [],
            loadingStatus: true,
            baseUrl: '',
            fam_id: '',
            person_id: '',
            latitude: '',
            longitude: '',
            username: '',
            password: '',
            connection_Status : '',
            settimeout:false,

            visibleEmail: false,
            visibleOtp: false,
            visibleNewPassword: false,

            passwordRe: '',
            passwordReCon: '',

            forgotEmail: '',
            forgetusername: '',
            forgetoldpassword:'',
            forgetnewpassword:''
        }
    }

    componentWillMount() {
        //this.profileDetails();
        // this.getBaseUrl();

    }
    // UpdateData (){
    //     // this.props.a("e");
    //     Actions.familyPhoto({ datas:this.state.response,photo:this.state.response.Photo })
      
    //   }
    checkNetwork=() => {
        NetInfo.isConnected.addEventListener(
          'connectionChange',
          this._handleConnectivityChange
    
      );
     
      NetInfo.isConnected.fetch().done((isConnected) => {
    
        if(isConnected == true)
        {
          this.setState({connection_Status : "Online"})
        }
        else
        {
          this.setState({connection_Status : "Offline"})
    
    
    
        }
    
      });
    
      }
    
    componentWillUnmount() {
        this.backHandler.remove();
        NetInfo.isConnected.removeEventListener(
            'connectionChange',
            this._handleConnectivityChange
     
        );
     
      }
     
      _handleConnectivityChange = (isConnected) => {
     
        if(isConnected == true)
          {
            this.setState({connection_Status : "Online"})
    
          }
          else
          {
            this.setState({connection_Status : "Offline"})
    
            
    
    
          }
      };
       
    componentDidMount() {
        this.checkNetwork();
        this.getBaseUrl();
        this.backHandler = BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);     

    }
   
    componentWillUnmount() {
        this.backHandler.remove();
      }
     
      handleBackPress = () => {
       
        if (Actions.currentScene !== '_Profile') {
            
        } else {
            this.setState({ visibleEmail: false })

            this.setState({ visibleOtp: false })
          Actions.pop();
      
          return true;
        }
      }
        
    renderResetPassword() {
      return (
          <View>
          <Dialog
              visible={this.state.visibleOtp}
              dialogTitle={<DialogTitle title="Reset Password" />}
              footer={
                  <DialogFooter>
                      <DialogButton
                      text="CANCEL"
                      onPress={() => { this.setState({ visibleOtp: false })}}
                      />
                      <DialogButton
                      text="SUBMIT"
                      onPress={() => { this.resetPasswordSubmitButton()}}
                      //onPress={() => { this.sendContactForm() }}
                      />
                  </DialogFooter>
                  }
              >
              <DialogContent>
  
                  <View style={{ width: 300, alignItems: 'center' }}>
  
                      <View style={{ marginTop: 30, width: 300, height: 40, backgroundColor: '#cfcdcc', justifyContent: 'center', borderRadius: 5 }}>
                          <TextInput 
                              placeholder='Enter Username'
                              value={this.state.forgetusername}
                              style={{ color: '#000', paddingLeft: 20 }}
                              onChangeText={(text) => this.setState({ forgetusername: text })}
                              autoCapitalize={false}
                              autoCorrect={false}
                          />
                          
                      </View>
                      <View style={{ marginTop: 30, width: 300, height: 40, backgroundColor: '#cfcdcc', justifyContent: 'center', borderRadius: 5 }}>
                          <TextInput 
                              placeholder='Enter Old Password'
                              value={this.state.forgetoldpassword}
                              style={{ color: '#000', paddingLeft: 20 }}
                              onChangeText={(text) => this.setState({ forgetoldpassword: text })}
                              autoCapitalize={false}
                              autoCorrect={false}
                          />
                          
                      </View>
                      <View style={{ marginTop: 30, width: 300, height: 40, backgroundColor: '#cfcdcc', justifyContent: 'center', borderRadius: 5 }}>
                          <TextInput 
                              placeholder='Enter new Password'
                              value={this.state.forgetnewpassword}
                              style={{ color: '#000', paddingLeft: 20 }}
                              onChangeText={(text) => this.setState({ forgetnewpassword: text })}
                              autoCapitalize={false}
                              autoCorrect={false}
                          />
                          
                      </View>
                      
      
                  </View>
                  {this.renderActivityIndicatorNew()}
              </DialogContent>
          </Dialog>
      </View>
      )
  }
  NewPassword = async () => {
    this.setState({ loadingIndicator: true });
    const { forgetusername, forgetnewpassword,forgetoldpassword,baseUrl,fam_id } = this.state;
    var endpoint = 'reset_password.php';
    var url = baseUrl + endpoint;
    var data = new FormData()
    data.append('hashcode', '##church00@');
    data.append('Fam_Id', fam_id);
    data.append('username', forgetusername);
    data.append('old_password', forgetoldpassword);
    data.append('new_password', forgetnewpassword);
    fetch(url, {
    method: 'POST',
    headers: {
        Accept: 'application/json',
        'Content-Type': 'multipart/form-data',
    },
    body: data,
    })
    .then((response) => response.json())
    .then((responseJson) => {
      Alert.alert(responseJson.Message);

        if (responseJson.status === 200) {
            this.setState({ visibleOtp: false, visibleNewPassword: true, loadingIndicator: false });
        } else {
            this.setState({ loadingIndicator: false });
        }
    })
    .catch((error) => {
      console.error(error);
    });
    setTimeout(() => {
        this.setState({
            loadingStatus:false,
            settimeout:true
        })
        },45000)
}

  resetPasswordSubmitButton() {
    if (this.state.forgetusername && this.state.forgetoldpassword && this.state.forgetnewpassword) {
        this.NewPassword();
    } else {
        Alert.alert('Enter all fields');
    }
}

    renderPhoneCheck() {
      return (
          <View>
          <Dialog
              visible={this.state.visibleEmail}
              dialogTitle={<DialogTitle title="Enter Valid Phone Number" />}
              footer={
                  <DialogFooter>
                      <DialogButton
                      text="CANCEL"
                      onPress={() => { this.setState({ visibleEmail: false })}}
                      />
                      <DialogButton
                      text="SUBMIT"
                      onPress={() => { this.phoneSubmitBtn() }}
                      />
                  </DialogFooter>
                  }
              >
              <DialogContent>
  
                  <View style={{ width: 300, alignItems: 'center' }}>
  
                      <View style={{ marginTop: 30, width: 300, height: 40, backgroundColor: '#cfcdcc', justifyContent: 'center', borderRadius: 5 }}>
                          <TextInput 
                              placeholder='Enter your Valid Phone Number'
                              value={this.state.forgotEmail}
                              style={{ color: '#000', paddingLeft: 20 }}
                              onChangeText={(text) => this.setState({ forgotEmail: text })}
                              autoCapitalize={false}
                              autoCorrect={false}
                          />
                          
                      </View>
                      {this.renderActivityIndicator()}
                  </View>
  
              </DialogContent>
          </Dialog>
      </View>
      )
  }
  phoneSubmitBtn() {
    if (this.state.forgotEmail) {
      this.PhoneCheckForPassword()
    } else {
        Alert.alert('Enter valid phone number');
    }
}
PhoneCheckForPassword = async () => {
  this.setState({ loadingIndicator: true });
    const { forgotEmail,fam_id ,baseUrl} = this.state;
    var endpoint = 'phone_check.php';
    const url = baseUrl+endpoint;
    var data = new FormData()
    data.append('hashcode', '##church00@');
         data.append('Fam_Id', fam_id);
        data.append('phone', forgotEmail);
        
    fetch(url, {
    method: 'POST',
    headers: {
        Accept: 'application/json',
        'Content-Type': 'multipart/form-data',
    },
    body: data,
    })
    .then((response) => response.json())
    .then((responseJson) => {
    Alert.alert(responseJson.Message)

        if (responseJson.status === 200) {
            this.setState({ visibleOtp: true, visibleEmail: false, loadingIndicator: false });

        } else {
            this.setState({ loadingIndicator: false });
        }
    })
    .catch((error) => {
      console.error(error);
    });

    setTimeout(() => {
        this.setState({
            loadingStatus:false,
            settimeout:true
        })
        },45000)

}
renderActivityIndicatorNew() {
    if (this.state.loadingStatus) {
        return (
                <ActivityIndicator size='small' />
        );

  }
  else if(this.state.loadingStatus==false && this.state.settimeout == true && this.state.connection_Status=='Offline' )
{
    return (
        <View>
        <Text >You are offline</Text>
        <TouchableOpacity onPress={() => this.NewPassword(this.state.baseUrl)}>
        <Icon style={{ marginLeft:40}} name="undo" size={17} color="#7BCFE8" />
        </TouchableOpacity>
        </View>
    )

}else{

}
}
renderActivityIndicator() {
    if (this.state.loadingStatus) {
        return (
                <ActivityIndicator size='small' />
        );
//   if (this.state.loadingIndicator) {
//       return (
//           <ActivityIndicator size='small' />
//       )
  }
  else if(this.state.loadingStatus==false && this.state.settimeout == true && this.state.connection_Status=='Offline' )
{
    return (
        <View>
        <Text >You are offline</Text>
        <TouchableOpacity onPress={() => this.PhoneCheckForPassword(this.state.baseUrl)}>
        <Icon style={{ marginLeft:40}} name="undo" size={17} color="#7BCFE8" />
        </TouchableOpacity>
        </View>
    )

}else{

}
}
callApiProfileUpdate(){
    this.getBaseUrl();
}
    async getBaseUrl() {
      try {
          let baseUrl = await AsyncStorage.getItem(DATA_BASE_URL);
          //let userDetaisl = await AsyncStorage.getItem(DATA_BASE_URL);
          var loginCredentials = await AsyncStorage.getItem(LOGIN_CREDENTIALS);
          var login_credentials = JSON.parse(loginCredentials);
          this.setState({ baseUrl: baseUrl, fam_id: login_credentials.family_id})
          var fam_id = login_credentials.family_id;
          this.setState({ baseUrl: baseUrl, person_id: login_credentials.person_id });
          this.callApiProfile(baseUrl, fam_id);
          
          
      } catch (error) {
          console.error('Something went Wrong in Get Section' + error);
      }
    }

      callApiProfile(baseUrl, fam_id) {

        //const { baseUrl, fam_id } = this.state;
        this.setState({ loadingStatus: true });
        var endpoint = 'profile.php';
        var url = baseUrl + endpoint;
        var data = new FormData();
        data.append('hashcode', '##church00@');
        data.append('Fam_Id', fam_id);
        fetch(url, {
        method: 'POST',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'multipart/form-data',
        },
        body: data,
        })
        .then((response) => response.json())
        .then((responseJson) => {

            if (responseJson.status === 200) {
              this.setState({ response: responseJson.response[0], loadingStatus: false, });
            }
            else {
            const error = 'Newtork error';
            }
        })
        .catch((error) => {
            console.error(error);
        });
    }


      logout() {
            Alert.alert(  
              'Logout',  
              'Do you want to logout',  
              [  
                  {  
                      text: 'Cancel',  
                      onPress: () => console.log('Cancel Pressed'),  
                      style: 'cancel',  
                  },  
                  {text: 'OK', onPress: () => this.clearAsyncStorage()},  
              ]  
          );  
      }

      clearAsyncStorage() {
        AsyncStorage.clear();
        Actions.ChooseChurch();
      }

      setLocation() {
        Alert.alert(  
          'Update Location',  
          'Are you sure, Now you are in home location',  
          [  
              {  
                  text: 'Cancel',  
                  onPress: () => console.log('Cancel Pressed'),  
                  style: 'cancel',  
              },  
              {text: 'OK', onPress: () => this.setLocationApi()},  
          ]  
      ); 
      }

      setLocationApi() {
        navigator.geolocation.getCurrentPosition((position) => {
            // this.setState({ 
            //   latitude: position.coords.latitude,
            //   longitude: position.coords.longitude
            //  })
             //console.log(position)

             this.apiEditLocation(position.coords.latitude, position.coords.longitude);
           
        },
            (error) => { console.log(error); },
            { enableHighAccuracy: true, timeout: 30000 }
        )
      }

      apiEditLocation(latitude, longitude) {

        const { baseUrl, person_id } = this.state;
        var endpoint = 'edit_location.php';
        var url = baseUrl + endpoint;
        var data = new FormData();
        data.append('hashcode', '##church00@');
        data.append('Person_Id', person_id);
        data.append('latitude', latitude);
        data.append('longitude', longitude);
        fetch(url, {
        method: 'POST',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'multipart/form-data',
        },
        body: data,
        })
        .then((response) => response.json())
        .then((responseJson) => {

            if (responseJson.status === 200) {
              //this.setState({ response: responseJson.response[0], loadingStatus: false });
              Alert.alert(responseJson.Message);
            }
            else {
            const error = 'Newtork error';
            }
        })
        .catch((error) => {
            console.error(error);
        });
    }

    

        render() {
            console.log('test 009');
            console.log(this.state.response);
            const { loadingStatus, response } = this.state;
            if(loadingStatus) {
              return (
                <View style={{ flex: 1, justifyContent: 'center',alignItems:'center'}}>
                   <ActivityIndicator size='large'/>
                </View>
               
              );
            } else {
              if (response.members[0].Photo) {
                var profPic =  { uri: this.state.baseUrl + response.members[0].Photo };
              } else {
                      var profPic = require('../../img/avatar.png');
                  } 
                  console.log(profPic);
            return(
             <ScrollView showsVerticalScrollIndicator={false}>
               <View style={{ backgroundColor:'#7BCFE8',height: 340, borderRadius: 20, borderTopLeftRadius: 0, borderTopRightRadius: 0}} >
{this.renderPhoneCheck()}
 {this.renderResetPassword()} 
                  <TouchableOpacity onPress={() => this.logout()}>
                    <View style={{ marginLeft: '75%', opacity: 0.6, position: 'relative', marginTop: 50, height: 30, width: 80, backgroundColor: '#000', borderRadius: 50, justifyContent: 'center', alignItems: 'center' }}>
                          <Text style={{ color: '#fff' }}>Logout</Text>
                    </View>
                  </TouchableOpacity>
                  <View style={{ justifyContent: 'center', alignItems: 'center', backgroundColor:'#7BCFE8', borderRadius: 20, borderTopLeftRadius: 0, borderTopRightRadius: 0}}>
                        <Image
                            style={{ height: 101, width: 101, borderRadius: 50 }}
                            source={profPic}
                        /> 
                        <Text style={{ fontSize: 22, fontWeight: 'bold', marginTop: 10}}>{response.Root}</Text>     
                        {/* <Text style={{ fontSize: 10, marginTop: 5 }}> {}</Text>  */}
                        <Text style={{ fontSize: 13, marginTop: 10 }}> {response.Family_name} House</Text> 
                    </View>
                    <View style={{ alignItems: 'center', width: '100%'}}>
                      <View style= {{ marginTop: 40, flexDirection: 'row'}}>
                              <TouchableOpacity onPress={() => Actions.familyPhoto({ datas: response ,parentCallback: this.callApiProfileUpdate})} style= {{ marginRight: 20, alignItems: 'center', justifyContent: 'center', borderRadius: 10, backgroundColor: '#fff', height: 30, width: 100,marginLeft:8}}>
                                  <Text style= {{fontSize: 9}}>Family Photo</Text>
                              </TouchableOpacity>
                               {/* <TouchableOpacity onPress={ this.UpdateData()} style= {{ marginRight: 20, alignItems: 'center', justifyContent: 'center', borderRadius: 10, backgroundColor: '#fff', height: 30, width: 100}}>
                                  <Text style= {{fontSize: 9}}>Family Photo</Text>
                              </TouchableOpacity> */}
                        
                              <TouchableOpacity onPress={() => this.setLocation()} style= {{ marginRight: 15, alignItems: 'center', justifyContent: 'center', borderRadius: 10, backgroundColor: '#fff', height: 30, width: 100}}>
                                  <Text style= {{fontSize: 9}}>Set Location</Text>
                              </TouchableOpacity>
                              <TouchableOpacity onPress={() => this.setState({visibleEmail:true})} style= {{ marginRight: 10, alignItems: 'center', justifyContent: 'center', borderRadius: 10, backgroundColor: '#fff', height: 30, width: 100}}>
                                  <Text style= {{fontSize: 9}}>Reset Password</Text>
                              </TouchableOpacity>
                              
                        </View>
                      </View>
                       
                </View>
                    <List abc={response.members} baseUrl={this.state.baseUrl}
                    parentCallback={ this.callApiProfileUpdate} />
                    

              </ScrollView>

            );
          }
        
        }
    }