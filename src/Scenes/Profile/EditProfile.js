import React, { Component } from 'react';
import { View, Text , Image, ScrollView, TouchableOpacity, TextInput,NetInfo, Alert,BackHandler, ActivityIndicator } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import ImagePicker from 'react-native-image-picker';
import { Actions } from 'react-native-router-flux';

export default class EditProfile extends Component {
    constructor() {
        super();
        this.state = {
            validated : false,
            image: '',
            imagevisiblilty:false,
            imageVisible: false,
            mobile: '',
            mobileVisible: false,
            imageValue: '',
            email_id: '',
            loadingStatus: false,
            emailVisible: false,
            connection_Status : '',
            settimeout:false,
            fam_id:'',
            abc:'',
image1:''
    
        }
    }

    componentDidMount() {
        this.checkNetwork();

        this.backHandler = BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);     

        if (this.props.datas.Phone == '0') {
            var mobileNumber = '';
        } else {
            var mobileNumber = this.props.datas.Phone;
        }
      
      
        
        this.setState({
            image1: this.props.baseUrl + this.props.datas.Photo,
            imageVisible: this.props.datas.photo_visibility,
            mobile: mobileNumber,
            mobileVisible: this.props.datas.phone_visibility,
            emailVisible:this.props.datas.email_visibility,
            email_id: this.props.datas.email,
        });
        console.log('profiles')
        console.log("sssed"+ JSON.stringify(this.props.datas) );
    }
    checkNetwork=() => {
        NetInfo.isConnected.addEventListener(
          'connectionChange',
          this._handleConnectivityChange
    
      );
     
      NetInfo.isConnected.fetch().done((isConnected) => {
    
        if(isConnected == true)
        {
          this.setState({connection_Status : "Online"})
        }
        else
        {
          this.setState({connection_Status : "Offline"})
    
    
    
        }
    
      });
    
      }
    
    componentWillUnmount() {
        this.backHandler.remove();
        NetInfo.isConnected.removeEventListener(
            'connectionChange',
            this._handleConnectivityChange
     
        );
     
      }
     
      _handleConnectivityChange = (isConnected) => {
     
        if(isConnected == true)
          {
            this.setState({connection_Status : "Online"})
    
          }
          else
          {
            this.setState({connection_Status : "Offline"})
    
            
    
    
          }
      };
       
       
       handleBackPress = () => {
        
         if (Actions.currentScene !== 'EditProfile') {
         } 
         else 
         {

             Actions.pop();

           return true;
         }
       }

    ProfilePictureAdd = () => {
        const options = {
            title: 'Select Avatar',
            storageOptions: {
              skipBackup: true,
              path: 'images',
            },
          };
          ImagePicker.showImagePicker(options, (response) => {
            console.log('Response = ', response);
          
            if (response.didCancel) {
              console.log('User cancelled image picker');
            } else if (response.error) {
              console.log('ImagePicker Error: ', response.error);
            } else {
              const source = { uri: response.uri };
          
              // You can also display the image using data:
              // const source = { uri: 'data:image/jpeg;base64,' + response.data };
              console.log(response);
          
              this.setState({
                //  abc:response.uri,
                  image: response.uri,
                imagevisiblilty:true
              });
            }
          })
    };
    showAlert(){
        Alert.alert(  
            'You are in offline',  
            [  
                {  
                    text: 'Cancel',  
                    onPress: () => console.log('Cancel Pressed'),  
                    style: 'cancel',  
                },  
                {text: 'Retry', onPress: () => this.checkNetwork()},  
            ]  
        );  
     }
    checkNetwork(){
        NetInfo.isConnected.addEventListener(
            'connectionChange',
            this._handleConnectivityChange
     
        );
       
        NetInfo.isConnected.fetch().done((isConnected) => {
     
          if(isConnected == true)
          {
            this.setState({connection_Status : "Online"})
            
          }
          else
          {
            this.setState({connection_Status : "Offline"})
            this.showAlert()
          }
     
        });

     }
     validate = (text) => {
        console.log(text);
        let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/ ;
        if(reg.test(text) === false)
        {
            Alert.alert(  
                ' ',  
                'Enter a valid Email id',  
                [  
                    {  
                       
                    },  
                    {text: 'ok', },  
                ]  
            ); 
        this.setState({email_id:text})
        return false;
          }
        else {
          this.setState({email_id:text,validate:true})
          this.updateProfileApiCall();
        }
    }
    updateProfile() {
        this.checkNetwork();
        const { email_id, imageVisible, mobileVisible, mobile ,emailVisible} = this.state;
        if(email_id){
            this.validate(email_id)

        }else{
            this.updateProfileApiCall();

        }

        // console.log(email + ' ' + imageVisible + ' ' + mobileVisible + ' ' + mobile);
        // if (imageVisible && mobileVisible) {
            // this.updateProfileApiCall();

        // } else {
        //     Alert.alert('Fields are empty');
        // }
    }
    UpdateData = () => {
        this.props.parentCallback("updated");
      
      }
    updateProfileApiCall() {
        
        this.setState({ loadingStatus: true })
        const { email_id, imageVisible, mobileVisible, mobile, image,emailVisible } = this.state;
        if (imageVisible) {
            var img_visible = 1
        } else {
            var img_visible = 0
        }

        if (mobileVisible) {
            var mob_visible = 1
        } else {
            var mob_visible = 0
        }
        if (emailVisible) {
            var em_visible = 1
        } else {
            var em_visible = 0
        }

        var endpoint = 'edit_profile.php';
        var url = this.props.baseUrl + endpoint;
        var data = new FormData();
        data.append('hashcode', '##church00@');
        data.append('Person_Id', this.props.datas.Person_Id);
        data.append('phone', mobile);
        data.append('email', email_id);
        data.append('email_visibility', em_visible);

        data.append('photo_visibility', img_visible);
        data.append('phone_visibility', mob_visible);
        if (image) {
            data.append('file', {
                //  uri: this.state.abc,
                  uri: image,
                type: 'image/jpg' || 'image/png',
                 name: 'person_photo.jpg'|| 'person_photo.png'
              });
        } else {
            data.append('file', '');
        }
        console.log(data);
        fetch(url, {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'multipart/form-data',
            },
            body: data,
            })
            .then((response) => response.json())
            .then((responseJson) => {
    
                if (responseJson.status === 200) {
                  //this.setState({ response: responseJson.response[0], loadingStatus: false });
                  this.setState({ loadingStatus: false })
                  Alert.alert(responseJson.Message);
                  this.UpdateData();
                  Actions.pop();


                }
                else {
                    this.setState({ loadingStatus: false })
                const error = 'Newtork error';
                }
            })
            
            .catch((error) => {
                console.error(error);
            });
            
            setTimeout(() => {
                this.setState({
                    loadingStatus:false,
                    settimeout:true
                })
                },45000)

        
    }

    renderUpdateButton() {
        if (this.state.loadingStatus) {
            return (
                
                
                    <View style={{ width: '100%'}}> 
                        <View style={{ backgroundColor: '#7BCFE8', width: '100%', height: 60, justifyContent: 'center', alignItems: 'center' }}>
                            <ActivityIndicator size='small' />
                        </View>
                    </View>
               
            )
        } else if(this.state.loadingStatus==false && this.state.settimeout == true && this.state.connection_Status=='Offline'  ){

    return (
        <TouchableOpacity onPress={() => this.updateProfile()}>
            <View style={{ width: '100%'}}> 
                <View style={{ backgroundColor: '#7BCFE8', width: '100%', height: 60, justifyContent: 'center', alignItems: 'center' }}>
                    <Text style={{ fontSize: 15, fontWeight: '500' }}> You are offline </Text>
                    <Icon style={{ marginRight:0}} name="undo" size={17} color="#fff" />

                </View>
            </View>
        </TouchableOpacity>
    )  
}else {
        
            return (
                <TouchableOpacity onPress={() => this.updateProfile()}>
                    <View style={{ width: '100%'}}> 
                        <View style={{ backgroundColor: '#7BCFE8', width: '100%', height: 60, justifyContent: 'center', alignItems: 'center' }}>
                            <Text style={{ fontSize: 15, fontWeight: '500' }}> UPDATE </Text>
                        </View>
                    </View>
                </TouchableOpacity>
            )
        }
    }

    render() {
        if (this.state.mobileVisible) {
            var icon_1 = 'eye';
        } else {
            var icon_1 = 'eye-slash';
        }

        if (this.state.imageVisible) {
            var icon_2 = 'eye';
        } else {
            var icon_2 = 'eye-slash';
        }
        if (this.state.emailVisible) {
            var icon_3 = 'eye';
        } else {
            var icon_3 = 'eye-slash';
        }
        
       
        // if (this.state.abc) {
        //     var profPic =  { uri: this.state.abc };
        // } else if (this.props.datas.Photo) {
        //         var profPic =  { uri: this.state.image1};
        //     } else {
        //         var profPic = require('../../img/avatar.png');
            
        // }
        if (this.state.image) {
            var profPic =  { uri: this.state.image };
        } else {
            if (this.props.datas.Photo) {
                var profPic =  { uri: this.props.baseUrl + this.props.datas.Photo };
            } else {
                var profPic = require('../../img/avatar.png');
            }
        }
        
        
       
        
        return(
            
            

            <View style={{ flex: 1 }}>
                <ScrollView showsVerticalScrollIndicator={false}>
                <View style={{ backgroundColor:'#7BCFE8',height: 320, borderRadius: 20, borderTopLeftRadius: 0, borderTopRightRadius: 0}} >
                    <View style={{ justifyContent: 'center', alignItems: 'center', backgroundColor:'#7BCFE8', borderRadius: 20, borderTopLeftRadius: 0, borderTopRightRadius: 0}}>
                        
                        <TouchableOpacity onPress={() => this.ProfilePictureAdd()}>
                            <Image
                                style={{ marginTop: 70, height: 101, width: 101, borderRadius: 50 }}
                                 source={profPic}
                                // source={{ uri: this.state.image1 }}
                            />
                        </TouchableOpacity>
                            <TouchableOpacity onPress={() => this.ProfilePictureAdd()}>
                                <View style={{ marginTop: -25, marginLeft: 60, height: 30, width: 30, borderRadius: 50, backgroundColor: '#808080', justifyContent: 'center', alignItems: 'center', }}>
                                    <Icon style={{ color: '#000'}} name='plus' size={20} />
                                </View> 
                            </TouchableOpacity>
            
                        <Text style={{ fontSize: 22, fontWeight: 'bold', marginTop: 10}}> {this.props.datas.Person_Name} </Text>     
                        {/* <Text style={{ fontSize: 10, marginTop: 5 }}> {}</Text>  */}
                        <Text style={{ fontSize: 13, marginTop: 10 }}> {this.props.datas.Relation}</Text> 
                    </View> 
                </View>

            <View style={{ alignItems: 'center', width: '100%' }}>

                <View style={{ backgroundColor: '#fff', height: 100, borderRadius: 10, marginTop: 20, width: '95%', }}>
                    <Text style={{ paddingTop: 10, paddingLeft: 20 }}>Profile Picture</Text>
                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                        <Text style={{ paddingTop: 20, paddingLeft: 20, width: '80%', paddingRight: 20, fontSize: 15, fontWeight: '300' }} >Visibility</Text>
                        <TouchableOpacity onPress={() => this.setState({ imageVisible: !this.state.imageVisible })}>
                            <Icon style={{ color: '#000', marginTop: 10, marginLeft: 10 }} name={icon_2} size={25} />
                        </TouchableOpacity>
                    </View>
                </View>

                <View style={{ backgroundColor: '#fff', height: 100, borderRadius: 10, marginTop: 20, width: '95%', }}>
                    <Text style={{ paddingTop: 10, paddingLeft: 20 }}>Mobile Number</Text>
                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                        <TextInput
                            style={{ paddingTop: 20, paddingLeft: 20, width: '80%', paddingRight: 20, fontSize: 15, fontWeight: '300' }} 
                            placeholder="Mobile Number"
                            keyboardType='number-pad'
                            value={this.state.mobile}
                            onChangeText={(text) => this.setState({ mobile: text })}
                        />
                        <TouchableOpacity onPress={() => this.setState({ mobileVisible: !this.state.mobileVisible })}>
                            <Icon style={{ color: '#000', marginTop: 10, marginLeft: 10 }} name={icon_1} size={25} />
                        </TouchableOpacity>
                    </View>

                </View>

                <View style={{ backgroundColor: '#fff', height: 100, borderRadius: 10, marginTop: 20, width: '95%', }}>
                    <Text style={{ paddingTop: 10, paddingLeft: 20 }}>Email</Text>
                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                        <TextInput 
                            style={{ paddingTop: 20, paddingLeft: 20, width: '80%', paddingRight: 20, fontSize: 15, fontWeight: '300', }} 
                            placeholder="Email id"
                            value={this.state.email_id}
                            autoCorrect={false}
                            autoCapitalize={'none'}
                            onChangeText={(text) => this.setState({ email_id: text })}
                        />
                         <TouchableOpacity onPress={() => this.setState({ emailVisible: !this.state.emailVisible })}>
                            <Icon style={{ color: '#000', marginTop: 10, marginLeft: 10 }} name={icon_3} size={25} />
                        </TouchableOpacity>
                    </View>
                </View>

            </View>
           

                </ScrollView>
                                {this.renderUpdateButton()}

                                </View>
        );
    }
}
    