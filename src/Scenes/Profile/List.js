import React, { Component } from 'react';
import { View, Text , Image, FlatList, TouchableOpacity } from 'react-native';
import { Actions } from 'react-native-router-flux';
import Icon from 'react-native-vector-icons/FontAwesome';



export default class List extends Component {
    constructor() {
        super();

        this.state = {
            baseUrl: 'https://cdn3.iconfinder.com/data/icons/business-avatar-1/512/',
    
        }
    }

renderList=({item}) => {

    if (item.Photo) {
        var profPic =  { uri: this.props.baseUrl + item.Photo };
    } else {
            var profPic = require('../../img/avatar.png');
        }
   
    return(
        <View style={{ justifyContent: 'center', backgroundColor: '#fff', marginTop: 10, marginLeft: 10,marginRight: 10, borderRadius: 10, height: 70}}>
        <View style={{ alignItems: 'center', flexDirection: 'row'}}>
            <Image 
              style={{ height: 45, width: 45, marginLeft: 10, borderRadius: 20 }}
              source={profPic}
            /> 
           <View style={{ marginLeft: 10, width: '65%' }} >
           
                <Text style={{ fontSize: 17, fontWeight: '500' }}>{item.Person_Name}</Text>              
                <Text style={{ fontSize: 10, paddingLeft: 2,  paddingTop: 5 }}>{item.job}</Text>
                                 
           </View>   
           <TouchableOpacity onPress={() => Actions.EditProfile({ datas: item, baseUrl: this.props.baseUrl, parentCallback: this.props.parentCallback})}>
                <View style={{ width: 50, marginLeft: 2, height:50, justifyContent: 'center'}}>
                    <Icon style={{}} name="edit" size={16} color="#000" />

                </View>  
            </TouchableOpacity>
        </View>    
                 
        </View>                          
    );
}

    render() {
        return(
                <View style= {{ backgroundColor: '#EBEBEB', marginTop: 10, borderRadius:20}}>
                    <FlatList
                        showsHorizontalScrollIndicator={false}
                        style={{marginTop: 10, marginBottom: 30}}                
                        data={this.props.abc}
                        renderItem={this.renderList}
                        keyExtractor={(item,index)=>index.toString()}
                    />
                </View>
        );
    }
}
    