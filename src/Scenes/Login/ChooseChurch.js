import React, { Component } from "react";
import { View, Text, TouchableOpacity, Image, ActivityIndicator,NetInfo, Alert ,BackHandler} from "react-native";
import { Actions} from 'react-native-router-flux';

export default class ChurchChoose extends Component {

  constructor() {
    super();

    this.state = {
      response_church: [],
      loadingStatus: false,
      connection_Status : "" 

    }
  }
  componentDidMount(){

  this.checkNetwork();
    this.backHandler = BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);     

  }

componentWillUnmount() {
  this.backHandler.remove();
 }
 
 handleBackPress = () => {
  
   if (Actions.currentScene !== 'ChooseChurch') {
   } 
   else 
   {
     
 BackHandler.exitApp()
 
     return true;
   }
 }

  abcTest() {
   
    this.churchListApi();
  }
  ShowAlertDialog=() =>{
 
    Alert.alert(
    
      'Internet Connection',
    'You are offline',
      [
       
   
        // Second Cancel Button in Alert Dialog.
        {text: 'Cancel', onPress: () => console.log('Cancel Button Pressed'), style: 'cancel'},
   
        // Third OK Button in Alert Dialog
        {text: 'retry',  onPress: () => this.checkNetwork() },
        
      ],
      {cancelable: false},
   
    )
   
  }
  
  checkNetwork=() => {
    NetInfo.isConnected.addEventListener(
      'connectionChange',
      this._handleConnectivityChange

  );
 
  NetInfo.isConnected.fetch().done((isConnected) => {

    if(isConnected == true)
    {
      this.setState({connection_Status : "Online"})
    }
    else
    {
      this.setState({connection_Status : "Offline"})

      this.ShowAlertDialog()      


    }

  });

  }
  componentWillUnmount() {

    NetInfo.isConnected.removeEventListener(
        'connectionChange',
        this._handleConnectivityChange
 
    );
 
  }
 
  _handleConnectivityChange = (isConnected) => {
 
    if(isConnected == true)
      {
        this.setState({connection_Status : "Online"})

      }
      else
      {
        this.setState({connection_Status : "Offline"})
        this.ShowAlertDialog()      

        


      }
  };
 


  churchListApi() {
    if(this.state.connection_Status==false){

    }else{
    this.setState({ loadingStatus: true });
    //var baseUrl = 'https://zero3.xyz/church/';
     var baseUrl = 'https://hamanahel.in/admin/';
    var endpoint = 'church.php';
    var url = baseUrl + endpoint;
    var data = new FormData();
    data.append('hashcode', '##church00@');
    fetch(url, {
    method: 'POST',
    headers: {
        Accept: 'application/json',
        'Content-Type': 'multipart/form-data',
    },
    body: data,
    })
    .then((response) => response.json())
    .then((responseJson) => {
      //console.log('seeet2 : ' + responseJson);
      //console.log(responseJson);
      if (responseJson.status === 200) {
        this.setState({ response_church: responseJson.response, loadingStatus: false })
        //Actions.ChurchList({ churchList: this.state.response_church })
        Actions.ChurchList({ churchList: responseJson.response })
      }
      else {
        const error = 'Check Internet connection';
        
        setTimeout(() => { this.ShowAlertDialog() }, 1000);
      }
    })
    .catch((error) => {
      console.error(error);
    });
  }
}

  render() {
    if (this.state.loadingStatus) {
      return (
        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
          <ActivityIndicator size='large' />
        </View>
      )
    }
    return (
        <View style={{ alignItems: 'center', height: '100%', backgroundColor: '#7BCFE8'}}>
            <View style={{marginTop:200, backgroundColor: '#fff', borderRadius: 70, width:140, height: 140, alignItems: 'center',  justifyContent: 'center', backgroundColor: '#fff'}}>
               <Image
                    source={require('/home/linux/Music/sahad_ezed-test_hamanahel-fbe83dbb0a4e/android/app/src/main/res/drawable/logo.png')}
                    style={{height:100, width:100, marginRight: 8, marginBottom: 8,marginLeft: 6, marginTop:8}}
               /> 
            </View>
            <View style={{marginTop:10}}>
            <View style={{ justifyContent: 'center', alignItems: 'center' }}>                                                                                                                                                                                                  
                        <Text style= {{ fontSize: 24, fontWeight: 'bold'}}>Hamanahel</Text>
            </View>
            </View>
            <View style={{marginTop:190}}>

            <TouchableOpacity onPress={() => this.churchListApi()}
            style={{ alignItems: 'center',  justifyContent: 'center', backgroundColor: '#7BCFE8', height: 40, width: 140, borderRadius: 25, marginTop: 10, borderColor: '#fff', borderWidth: 2}}>
                    <Text style={{fontWeight: 'normal', color: '#fff'}}>Choose Church</Text>
            </TouchableOpacity>
            </View>
        </View>
    );
  }
}