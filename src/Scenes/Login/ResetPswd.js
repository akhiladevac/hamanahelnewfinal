/* eslint-disable prettier/prettier */
import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity, TextInput,NetInfo, Alert, ActivityIndicator } from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import Icon from 'react-native-vector-icons/FontAwesome';
import { Actions } from 'react-native-router-flux';

    const DATA_BASE_URL = '@base_url';
    const LOGIN_CREDENTIALS = '@login_credentials';
    export default class ResetPswd extends Component{

        constructor() {
            super();

            this.state= {
                tempusername: '',                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           
                password:'',
                confirmPssword:'',
                countrySelected: true,
                dataSource:[],
                data:'',
                baseUrl: '',
                loadingStatus: false,
            }
        }
       
resetPwd(){
    if(this.state.tempusername == "" )
    {
        alert("Enter Username");
    }else if(this.state.password==""){
        alert("Enter old password");

    }
    else if(this.state.confirmPssword==""){
        alert("Enter new password");

    }
    else{
    this.getBaseUrl();
    }

}
       
        async getBaseUrl() {
            try {
                let baseUrl = await AsyncStorage.getItem(DATA_BASE_URL);
                //let userDetaisl = await AsyncStorage.getItem(DATA_BASE_URL);
                var loginCredentials = await AsyncStorage.getItem(LOGIN_CREDENTIALS);
                var login_credentials = JSON.parse(loginCredentials);
                //this.setState({ baseUrl: baseUrl, fam_id: login_credentials.family_id})
                var fam_id = login_credentials.family_id;
                
                this.resetPassword(baseUrl,fam_id)
               
                
            } catch (error) {
                console.error('Something went Wrong in Get Section' + error);
            }
          
        }
        resetPassword(baseUrl,fam_id) {

         
          
         
              
                var endpoint = 'reset_password.php';
                var url = baseUrl + endpoint;
                var data = new FormData();
                data.append('hashcode', '##church00@');
                data.append('Fam_Id', fam_id);
                data.append('username', this.state.tempusername);
                data.append('old_password', this.state.password);
                data.append('new_password', this.state.confirmPssword);

                fetch(url, {
                    method: 'POST',
                    headers: {
                        Accept: 'application/json',
                        'Content-Type': 'multipart/form-data',
                    },
                    body: data,
                    })
                    .then((response) => response.json())
                    .then((responseJson) => {
                  
                       
                        alert(responseJson.Message)
                      
                       
                    })
                    .catch((error) => {
                        
                   });
              
                  }
                
                
        render(){
           
            if (this.state.loadingStatus) {
                return (
                    <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                        <ActivityIndicator size='large' />
                    </View>
                )
            } 
            return(
                
                <View style={{ backgroundColor: '#fff', height: '100%', width: '100%', alignItems: 'center' }}>
                    <View style={{ marginTop: 100 }}>
                    <View style={{ justifyContent: 'center', alignItems: 'center' }}>                                                                                                                                                                                                  
                        <Text style= {{ fontSize: 23, fontWeight: 'bold', marginTop:0 }}>Reset Password</Text>     
                        <Text style= {{ fontSize: 15, marginTop: 5 }}> </Text>
                    </View>

                    <View style= {styles.login}>
                        <Icon style={{ marginLeft: 20}} name="user" size={20} color="#000" />
                        <TextInput
                         autoCapitalize="none"
                         style={{ color: '#000', fontSize: 16, fontWeight: '400', paddingLeft: 20 }}
                         value={this.state.username}
                         placeholder='username'
                         onChangeText={tempusername => this.setState({ tempusername})}
                        />
                        
                    </View>
                    <View style={styles.password}>
                    <Icon style={{ marginLeft: 20}} name="lock" size={20} color="#000" />

                        <TextInput
                            autoCapitalize="none"
                            secureTextEntry
                            style={{ color: '#000', fontSize: 16, fontWeight: '400', paddingLeft: 20 }}
                            value={this.state.password}
                            placeholder="*********" 
                            onChangeText={password => this.setState({ password})}
                        />

                    </View> 
                    <View style={styles.password}>
                    <Icon style={{ marginLeft: 20}} name="lock" size={20} color="#000" />

                        <TextInput
                            autoCapitalize="none"
                            secureTextEntry
                            style={{ color: '#000', fontSize: 16, fontWeight: '400', paddingLeft: 20 }}
                            value={this.state.confirmPssword}
                          
                            onChangeText={confirmPssword => this.setState({ confirmPssword})}
                        />

                    </View>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       
                    <TouchableOpacity 
                        onPress={() => this.resetPwd()}
                        style={{alignItems: 'center', justifyContent: 'center', backgroundColor: '#45788f', width: 160, height: 40, marginTop:25, borderRadius: 15, marginLeft:130}}>
                        <Text style={{color: '#ffff'}}>Reset Password</Text>

                    </TouchableOpacity>
                   
                    
                                
                     
                    </View>    
                </View>
            )
        }
    }

    styles = StyleSheet.create({
        login: {
            marginTop: 20,
            borderRadius: 20, 
            backgroundColor: '#7BCFE8', 
            width: 320, 
            height: 64, 
            alignItems: 'center' ,
            flexDirection: 'row',
        },
        password: {
            marginTop: 20,
            borderRadius: 20, 
            backgroundColor: '#69aabf', 
            width: 320, 
            height: 64, 
            alignItems: 'center' ,
            flexDirection: 'row',
            
        },
        text: {
            paddingLeft: 25,
            fontSize: 18,
            fontWeight: '300',
            color: '#000',
            width: '100%'
        }
    })