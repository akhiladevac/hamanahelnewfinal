/* eslint-disable prettier/prettier */
import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity, TextInput, NetInfo, Alert, ActivityIndicator, BackHandler, Platform } from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import Icon from 'react-native-vector-icons/FontAwesome';
import RNExitApp from 'react-native-exit-app';
import { Actions } from 'react-native-router-flux';
import Dialog, { DialogTitle, DialogFooter, DialogButton, DialogContent } from 'react-native-popup-dialog';
const DATA_LIST_MEMBERS = '@members_list';


    const DATA_BASE_URL = '@base_url';
    const LOGIN_CREDENTIALS = '@login_credentials';
    export default class Login extends Component{

        constructor() {
            super();

            this.state= {
                response:[],
                username: '',                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           
                password:'',
                countrySelected: true,
                dataSource:[],
                data:'',
                baseUrl: '',
                loadingStatus: false,
                connection_Status : "",
                Alert_Visibility: false ,
                visibleEmail: false,
                visibleOtp: false,
                visiblePassword: false,

                visibleNewPassword: false,
                response:[],
                passwordRe: '',
                passwordReCon: '',
    
                forgotEmail: '',
                forgetusername: '',
                forgetNewpassword: '',

                forgetoldpassword:'',
                forgetnewpassword:'',
                fam_id:'',
                settimeout:false,

            }
        }
        // generateotp(){
        //     Actions.GenerateOTP();
        // }
        renderGenerateOTP() {
            return (
                <View>
                <Dialog
                    visible={this.state.visibleEmail}
                    dialogTitle={<DialogTitle title="Enter Valid Email id" />}
                    footer={
                        <DialogFooter>
                            <DialogButton
                            text="CANCEL"
                            onPress={() => { this.setState({ visibleEmail: false })}}
                            />
                            <DialogButton
                            text="SUBMIT"
                            onPress={() => { this.emailSubmitBtn() }}
                            />
                        </DialogFooter>
                        }
                    >
                    <DialogContent>
        
                        <View style={{ width: 300, alignItems: 'center' }}>
        
                            <View style={{ marginTop: 30, width: 300, height: 40, backgroundColor: '#cfcdcc', justifyContent: 'center', borderRadius: 5 }}>
                                <TextInput 
                                    placeholder='Enter your Valid Email id'
                                    value={this.state.forgotEmail}
                                    style={{ color: '#000', paddingLeft: 20 }}
                                    onChangeText={(text) => this.setState({ forgotEmail: text })}
                                    autoCapitalize={false}
                                    autoCorrect={false}
                                />
                                
                            </View>
                            {this.renderActivityIndicator1()}
                        </View>
        
                    </DialogContent>
                </Dialog>
            </View>
            )
        }
        renderActivityIndicator1() {
            if (this.state.loadingStatus) {
                return (
                        <ActivityIndicator size='small' />
                );
            }
            else if(this.state.loadingStatus==false && this.state.settimeout == true && this.state.connection_Status=='Offline' )
            {
                return (
                    <View style={{ justifyContent:'center',alignItems:'center'}}>
                    <Text >You are offline</Text>
                    <TouchableOpacity onPress={() => this.GenerateOtpForPassword()}>
                    <Icon style={{ }} name="undo" size={17} color="#7BCFE8" />
                    </TouchableOpacity>
                    </View>
                )
            }else{
        
            }
          }
          renderActivityIndicator2() {
            if (this.state.loadingStatus) {
                return (
                        <ActivityIndicator size='small' />
                );
            }
            else if(this.state.loadingStatus==false && this.state.settimeout == true && this.state.connection_Status=='Offline' )
            {
                return (
                    <View style={{ justifyContent:'center',alignItems:'center'}}>
                    <Text >You are offline</Text>
                    <TouchableOpacity onPress={() => this.VerifyOtp()}>
                    <Icon style={{ }} name="undo" size={17} color="#7BCFE8" />
                    </TouchableOpacity>
                    </View>
                )
            }else{
        
            }
          }
          
        GenerateOtpForPassword = async () => {
            this.setState({ loadingIndicator: true });
              const { forgotEmail,baseUrl} = this.state;
              var endpoint = 'generate_otp.php';
              const url = baseUrl+endpoint;
              var data = new FormData()
              data.append('hashcode', '##church00@');
                  data.append('email', forgotEmail);
                  
              fetch(url, {
              method: 'POST',
              headers: {
                  Accept: 'application/json',
                  'Content-Type': 'multipart/form-data',
              },
              body: data,
              })
              .then((response) => response.json())
              .then((responseJson) => {
               Alert.alert(responseJson.Message);
      
                  if (responseJson.status === 200) {
                      this.setState({ visibleOtp: true, visibleEmail: false, loadingIndicator: false });
                  } else {
                      this.setState({ loadingIndicator: false });
                  }
              })
              .catch((error) => {
                console.error(error);
              });
          
              setTimeout(() => {
                this.setState({
                    loadingStatus:false,
                    settimeout:true
                })
                },45000)
          
        }
        VerifyOtp = async () => {
            this.setState({ loadingIndicator: true });
              const { forgetusername,baseUrl,forgotEmail} = this.state;
              var endpoint = 'verify_otp.php';
              const url = baseUrl+endpoint;
              var data = new FormData()
              data.append('hashcode', '##church00@');
                  data.append('email', forgotEmail);
                  data.append('otp', forgetusername);
                  
              fetch(url, {
              method: 'POST',
              headers: {
                  Accept: 'application/json',
                  'Content-Type': 'multipart/form-data',
              },
              body: data,
              })
              .then((response) => response.json())
              .then((responseJson) => {
                Alert.alert(responseJson.Message)
      
                  if (responseJson.status === 200) {
                      this.setState({ visiblePassword: true, visibleOtp: false, loadingIndicator: false });
                  } else {
                      this.setState({ loadingIndicator: false });
                  }
              })
              .catch((error) => {
                console.error(error);
              });
          
              setTimeout(() => {
                this.setState({
                    loadingStatus:false,
                    settimeout:true
                })
                },45000)
          
        }
        GenerateNewPassword = async () => {
            this.setState({ loadingIndicator: true });
              const { forgetNewpassword,baseUrl,forgotEmail} = this.state;
              var endpoint = 'new_password.php';
              const url = baseUrl+endpoint;
              var data = new FormData()
              data.append('hashcode', '##church00@');
                  data.append('email', forgotEmail);
                  data.append('password', forgetNewpassword);
                  
              fetch(url, {
              method: 'POST',
              headers: {
                  Accept: 'application/json',
                  'Content-Type': 'multipart/form-data',
              },
              body: data,
              })
              .then((response) => response.json())
              .then((responseJson) => {
               Alert.alert(responseJson.Message)
      
                  if (responseJson.status === 200) {
                      this.setState({ visiblePassword: false, visibleOtp: false, loadingIndicator: false });
                  } else {
                      this.setState({ loadingIndicator: false });
                  }
              })
              .catch((error) => {
                console.error(error);
              });
              setTimeout(() => {
                this.setState({
                    loadingStatus:false,
                    settimeout:true
                })
                },45000)
      
          
        }
        resetPasswordSubmitButton() {
            if (this.state.forgetusername) {
              this.VerifyOtp()
            } else {
                Alert.alert('Enter valid OTP');
            }
        }
        newPasswordSubmitButton() {
            if (this.state.forgetNewpassword) {
              this.GenerateNewPassword()
            } else {
                Alert.alert('Enter new Password');
            }
        }
        emailSubmitBtn() {
            if (this.state.forgotEmail) {
              this.GenerateOtpForPassword()
            } else {
                Alert.alert('Enter valid Email Id');
            }
        }
        renderActivityIndicator() {
            if (this.state.loadingStatus) {
                return (
                        <ActivityIndicator size='small' />
                );
            }
            else if(this.state.loadingStatus==false && this.state.settimeout == true && this.state.connection_Status=='Offline' )
            {
                return (
                    <View style={{ justifyContent:'center',alignItems:'center'}}>
                    <Text >You are offline</Text>
                    <TouchableOpacity onPress={() => this.GenerateNewPassword()}>
                    <Icon style={{ }} name="undo" size={17} color="#7BCFE8" />
                    </TouchableOpacity>
                    </View>
                )
            }else{
        
            }
        }
     showAlert(){
        Alert.alert(  
            'You are in offline',  
            [  
                {  
                    text: 'Cancel',  
                    onPress: () => console.log('Cancel Pressed'),  
                    style: 'cancel',  
                },  
                {text: 'Retry', onPress: () => this.checkNetwork()},  
            ]  
        );  
     }
     handleBackPress = () => {
                
        if (Actions.currentScene !== 'Login') {
        } 
        else 
        {
            this.setState({ visibleEmail: false })
            this.setState({ visibleOtp: false })
            this.setState({ visiblePassword: false })


            Actions.pop();
 
      
          return true;
        }
      }
          componentWillUnmount() {
           
                this.backHandler.remove();
               
               
            
         
            NetInfo.isConnected.removeEventListener(
                'connectionChange',
                this._handleConnectivityChange
         
            );
         
          }
         
          _handleConnectivityChange = (isConnected) => {
         
            if(isConnected == true)
              {
                this.setState({connection_Status : "Online"})
              }
              else
              {
                this.setState({connection_Status : "Offline"})
                this.showAlert()
              }
          };
         checkNetwork(){
            NetInfo.isConnected.addEventListener(
                'connectionChange',
                this._handleConnectivityChange
         
            );
           
            NetInfo.isConnected.fetch().done((isConnected) => {
         
              if(isConnected == true)
              {
                this.setState({connection_Status : "Online"})
                
              }
              else
              {
                this.setState({connection_Status : "Offline"})
                this.showAlert()
              }
         
            });

         }
        componentDidMount() {
            //check the network
            console.log("Login");
 
            this.checkNetwork();
             this.getBaseUrl();
             this.backHandler = BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);     

        }

        async getBaseUrl() {
            try {
                let baseUrl = await AsyncStorage.getItem(DATA_BASE_URL);
                this.setState({ baseUrl: baseUrl });
                
            } catch (error) {
                console.error('Something went Wrong in Get Section' + error);
            }
        }
        callApiMembers(baseUrl,fam_id) {
            
            this.setState({ loadingStatus: true });
            var endpoint = 'list_of_members.php';
            var url = baseUrl + endpoint;
            var data = new FormData();
            data.append('hashcode', '##church00@');
            data.append('Fam_Id', fam_id);
            fetch(url, {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'multipart/form-data',
            },
            body: data,
            })
            .then((response) => response.json())
            .then((responseJson) => {
                
                if (responseJson.status === 200) {
                this.storeItems(responseJson.response, DATA_LIST_MEMBERS);
                }
                else {
                const error = 'Async Storage Failed';
                }
            })
            .catch((error) => {
                console.error(error);
            });
        }
        
        //this function for save the array response of members list to AsyncStorage
        async storeItems(response, KEY) {
            await AsyncStorage.setItem(KEY, JSON.stringify(response));
            this.setState({ loadingStatus: false });
        }
    
        async checkUserIsLoggedIn() {
            try {
                var loginCredentials = await AsyncStorage.getItem(LOGIN_CREDENTIALS);
              
                    //console.log(loginCredentials);
                    var login_credentials = JSON.parse(loginCredentials);
                    //console.log(login_credentials.role);
                    this.setState({ userRole: login_credentials.role ,fam_id:login_credentials.family_id});
                
            } catch (error) {
                console.error('Something went Wrong in Get Section' + error);
            }
        }
priestUserlogin(){
    const { username, password, baseUrl } = this.state;
    this.setState({ loadingStatus: true });
    var str = this.state.username;
    var res1 = str.substring(0, 7);
    var res=res1.toLowerCase();
    if(res =='priest_')
    {

        var endpoint = 'priest_login.php';
      var url = baseUrl + endpoint;
      console.log(url);
      var data = new FormData();
        data.append('hashcode', '##church00@');
        data.append('username', username);
        data.append('password', password);
        fetch(url, {
     method: 'POST',
     headers: {
        Accept: 'application/json',
        'Content-Type': 'multipart/form-data',
     },
        body: data,
     })
    .then((response) => response.json())
    .then((responseJson) => {
    console.log('seeet2 : ' + responseJson);
    console.log(responseJson);
    if (responseJson.status === 200) {
var userDetails = { 
                "username": this.state.username,
                "password": this.state.password,
                "family_id": responseJson.response[0].Fam_Id,
                "role": responseJson.response[0].Role,
            "person_id" : responseJson.response[0].members[0].Person_Id,
            "phone" :responseJson.response[0].Phone
                
                }
this.storeUserDetails(userDetails);

                                     
            Alert.alert(
'Exit App',
'Please Close the App',
[

{text: 'ok', onPress: () => this.exitApp()},
],
{ cancelable: false });
// }
    
} 
else {
        const error = 'Invalid username or password';
        Alert.alert(error);
        this.setState({ loadingStatus: false });
    }
    })
    .catch((error) => {
    console.error(error);
    });

}else{
    var endpoint = 'login.php';
    var url = baseUrl + endpoint;
    console.log(url);
    var data = new FormData();
    data.append('hashcode', '##church00@');
    data.append('username', username);
    data.append('password', password);
        fetch(url, {
    method: 'POST',
    headers: {
        Accept: 'application/json',
        'Content-Type': 'multipart/form-data',
    },
    body: data,
    })
    .then((response) => response.json())
    .then((responseJson) => {
    console.log('seeet2 : ' + responseJson);
    console.log(responseJson);
    if (responseJson.status === 200) {

        var userDetails = { 
                            "username": this.state.username,
                            "password": this.state.password,
                            "family_id": responseJson.response[0].Fam_Id,
                            "role": responseJson.response[0].Role,
                        "person_id" : responseJson.response[0].members[0].Person_Id,
                        "phone" :responseJson.response[0].Phone
                            
                            }

               this.storeUserDetails(userDetails);
  } 
  else {
    const error = 'Invalid username or password';
    Alert.alert(error);
    this.setState({ loadingStatus: false });
}
})
.catch((error) => {
console.error(error);
});

 }
}
            





        login() {
            if ( this.state.username && this.state.password ) 
            {
                this.priestUserlogin();
//                 const { username, password, baseUrl } = this.state;
//                 this.setState({ loadingStatus: true });
//                 var str = this.state.username;

//                 var res = str.substring(0, 7);
//                 if(res =='priest_')
//                 {
//                     var endpoint = 'priest_login.php';
//                 var url = baseUrl + endpoint;
//                 console.log(url);
//                 var data = new FormData();
//                 data.append('hashcode', '##church00@');
//                 data.append('username', username);
//                 data.append('password', password);
//                 fetch(url, {
//                 method: 'POST',
//                 headers: {
//                     Accept: 'application/json',
//                     'Content-Type': 'multipart/form-data',
//                 },
//                 body: data,
//                 })
//                 .then((response) => response.json())
//                 .then((responseJson) => {
//                 console.log('seeet2 : ' + responseJson);
//                 console.log(responseJson);
//                 if (responseJson.status === 200) {

                   

//                     //var role = responseJson.response.Role;
//                     //var fam_id = responseJson.response.Fam_Id;
//                     //console.log(role + ' ' + fam_id);

//                     var userDetails = { 
//                             "username": this.state.username,
//                             "password": this.state.password,
//                             "family_id": responseJson.response[0].Fam_Id,
//                             "role": responseJson.response[0].Role,
//                         "person_id" : responseJson.response[0].members[0].Person_Id,
//                         "phone" :responseJson.response[0].Phone
                            
//                             }
//     // this.callApiMembers(this.state.baseUrl,this.state.fam_id);

       
//         // console.log(userDetails);
//         this.storeUserDetails(userDetails);
                                        
                                    

//                     // if(responseJson.response[0].Role === 'priest'){
                     
//                         Alert.alert(
//     'Exit App',
//     'Please Close the App',
//     [
    
//       {text: 'ok', onPress: () => this.exitApp()},
//     ],
//     { cancelable: false });
// // }
                
//  } 
//  else {
//                     const error = 'Invalid username or password';
//                     Alert.alert(error);
//                     this.setState({ loadingStatus: false });
//                 }
//                 })
//                 .catch((error) => {
//                 console.error(error);
//                 });
//             }
// //             } else {
// //                 Alert.alert('Login fields are empty');
// //             }
        
    

//                 }else {
//                     const { username, password, baseUrl } = this.state;

//                 var endpoint = 'login.php';
//                 var url = baseUrl + endpoint;
//                 console.log(url);
//                 var data = new FormData();
//                 data.append('hashcode', '##church00@');
//                 data.append('username', username);
//                 data.append('password', password);
//                 alert(username)
//                 fetch(url, {
//                 method: 'POST',
//                 headers: {
//                     Accept: 'application/json',
//                     'Content-Type': 'multipart/form-data',
//                 },
//                 body: data,
//                 })
//                 .then((response) => response.json())
//                 .then((responseJson) => {
//                 console.log('seeet2 : ' + responseJson);
//                 console.log(responseJson);
//                 if (responseJson.status === 200) {

                   

//                     //var role = responseJson.response.Role;
//                     //var fam_id = responseJson.response.Fam_Id;
//                     //console.log(role + ' ' + fam_id);

//                     var userDetails = { 
//                                         "username": this.state.username,
//                                         "password": this.state.password,
//                                         "family_id": responseJson.response[0].Fam_Id,
//                                         "role": responseJson.response[0].Role,
//                                     "person_id" : responseJson.response[0].members[0].Person_Id,
//                                     "phone" :responseJson.response[0].Phone
                                        
//                                         }
//                 // this.callApiMembers(this.state.baseUrl,this.state.fam_id);

                   
//                     // console.log(userDetails);
//                     this.storeUserDetails(userDetails);

// //                     if(responseJson.response[0].Role === 'priest'){
                     
// //                         Alert.alert(
// //     'Exit App',
// //     'Please Close the App',
// //     [
    
// //       {text: 'ok', onPress: () => this.exitApp()},
// //     ],
// //     { cancelable: false });
// // }
                




                       
//                 } else {
//                     const error = 'Invalid username or password';
//                     Alert.alert(error);
//                     this.setState({ loadingStatus: false });
//                 }
//                 })
//                 .catch((error) => {
//                 console.error(error);
//                 });
            } else {
                Alert.alert('Login fields are empty');
             }
    
        
    }
     phoneCheck(){
        var endpoint = 'phone_check.php';
       var url = baseUrl + endpoint;
        // console.log(url);
        var data = new FormData();
        data.append('hashcode', '##church00@');
        data.append('Fam_Id',)
        data.append('phone')
        fetch(url, {
        method: 'POST',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'multipart/form-data',
        },
        body: data,
        })
        .then((response) => response.json())
        .then((responseJson) => {
            this.setState({ response: responseJson.response, loadingStatus: false});
            // console.log(responseJson);
        })
        .catch((error) => {
          console.error(error);
        });
    

     }
     renderVerifyOtp() {
        return (
            <View>
            <Dialog
                visible={this.state.visibleOtp}
                dialogTitle={<DialogTitle title="Verify Otp "/>}
                footer={
                    <DialogFooter>
                        <DialogButton
                        text="CANCEL"
                        onPress={() => { this.setState({ visibleOtp: false })}}
                        />
                        <DialogButton
                        text="SUBMIT"
                        onPress={() => { this.resetPasswordSubmitButton()}}
                        />
                    </DialogFooter>
                    }
                >
                <DialogContent>
    
                    <View style={{ width: 300, alignItems: 'center' }}>
    
                        <View style={{ marginTop: 30, width: 300, height: 40, backgroundColor: '#cfcdcc', justifyContent: 'center', borderRadius: 5 }}>
                            <TextInput 
                                placeholder='Enter OTP'
                                value={this.state.forgetusername}
                                style={{ color: '#000', paddingLeft: 20 }}
                                onChangeText={(text) => this.setState({ forgetusername: text })}
                                autoCapitalize={false}
                                autoCorrect={false}
                            />
                            
                        </View>
                        
                      
                    </View>
                    {this.renderActivityIndicator2()}
                </DialogContent>
            </Dialog>
        </View>
        )
    }
    renderNewPassword() {
        return (
            <View>
            <Dialog
                visible={this.state.visiblePassword}
                dialogTitle={<DialogTitle title="Generate New Password "/>}
                footer={
                    <DialogFooter>
                        <DialogButton
                        text="CANCEL"
                        onPress={() => { this.setState({ visiblePassword: false })}}
                        />
                        <DialogButton
                        text="SUBMIT"
                        onPress={() => { this.newPasswordSubmitButton()}}
                        />
                    </DialogFooter>
                    }
                >
                <DialogContent>
    
                    <View style={{ width: 300, alignItems: 'center' }}>
    
                        <View style={{ marginTop: 30, width: 300, height: 40, backgroundColor: '#cfcdcc', justifyContent: 'center', borderRadius: 5 }}>
                            <TextInput 
                                placeholder='Enter New Password'
                                value={this.state.forgetNewpassword}
                                style={{ color: '#000', paddingLeft: 20 }}
                                onChangeText={(text) => this.setState({ forgetNewpassword: text })}
                                autoCapitalize={false}
                                autoCorrect={false}
                            />
                            
                        </View>
                        </View>
                    {this.renderActivityIndicator()}
                </DialogContent>
            </Dialog>
        </View>
        )
    }
   
        exitApp() {
            if (Platform.OS === 'ios') {
                RNExitApp.exitApp();
            } else {
                BackHandler.exitApp();
            }
            
        }


        async storeUserDetails(userDetails) {
            await AsyncStorage.setItem(LOGIN_CREDENTIALS, JSON.stringify(userDetails));
            //this.setState({ loadingStatus: false });
              Actions.Home();
            // Actions.Userlist();
//   BackHandler.exitApp();
          }
         

        renderItems() {
            if(this.state.countrySelected === true) {
                return(
                    <View>
                    <TouchableOpacity onPress={() => this.setState({visibleEmail:true})} style={{ marginRight: 190, marginTop: -30}}>
                    <Text style={{color: '#7BCFE8', fontWeight: "bold"}}>Forgot Password?</Text>
                    </TouchableOpacity>
                    
                    {/* <TouchableOpacity  onPress={() => this.phoneCheck()} style={{ marginLeft: 100, marginTop: 30}}>
                    <Text style={{color: '#7BCFE8', fontWeight: "bold"}}>Reset Password</Text>
                    </TouchableOpacity> */}
                    </View>
                )
            }
            
        }

        render(){
            const { church_name, place } = this.props.church_details;
            if (this.state.loadingStatus) {
                return (
                    <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                        <ActivityIndicator size='large' />
                    </View>
                )
            } 
            return(
               
                <View style={{ backgroundColor: '#fff', height: '100%', width: '100%', alignItems: 'center' }}>
                     {this.renderGenerateOTP()}
                    {this.renderVerifyOtp()} 
                    {this.renderNewPassword()} 
                    <TouchableOpacity onPress={() => Actions.Login()}>
              </TouchableOpacity>

                    <View style={{ marginTop: 100 }}>
                    <View style={{ justifyContent: 'center', alignItems: 'center' }}>                                                                                                                                                                                                  
                        <Text style= {{ fontSize: 23, fontWeight: 'bold', marginTop:0 }}>{church_name} Church</Text>     
                        <Text style= {{ fontSize: 15, marginTop: 5 }}> {place}</Text>
                    </View>

                    <View style= {styles.login}>
                        <Icon style={{ marginLeft: 20}} name="user" size={20} color="#000" />
                        <TextInput
                         autoCapitalize="none"
                         style={{ color: '#000', fontSize: 16, fontWeight: '400', paddingLeft: 20 }}
                         value={this.state.username}
                         placeholder='username'
                         onChangeText={username => this.setState({ username})}
                        />
                        
                    </View>
                    <View style={styles.password}>
                    <Icon style={{ marginLeft: 20}} name="lock" size={20} color="#000" />

                        <TextInput
                            autoCapitalize="none"
                            secureTextEntry
                            style={{ color: '#000', fontSize: 16, fontWeight: '400', paddingLeft: 20 }}
                            value={this.state.password}
                            placeholder="*********" 
                            onChangeText={password => this.setState({ password})}
                        />

                    </View>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 
                    <TouchableOpacity 
                        onPress={() => this.login()}
                        style={{alignItems: 'center', justifyContent: 'center', backgroundColor: '#45788f', width: 160, height: 40, marginTop:25, borderRadius: 15, marginLeft:130}}>
                        <Text style={{color: '#ffff'}}>Login</Text>

                    </TouchableOpacity>
                   
                    
                                  {this.renderItems()} 
                     
                    </View>    
                </View>
            )
        }
    }

    styles = StyleSheet.create({
        login: {
            marginTop: 20,
            borderRadius: 20, 
            backgroundColor: '#7BCFE8', 
            width: 320, 
            height: 64, 
            alignItems: 'center' ,
            flexDirection: 'row',
        },
        password: {
            marginTop: 20,
            borderRadius: 20, 
            backgroundColor: '#69aabf', 
            width: 320, 
            height: 64, 
            alignItems: 'center' ,
            flexDirection: 'row',
            
        },
        text: {
            paddingLeft: 25,
            fontSize: 18,
            fontWeight: '300',
            color: '#000',
            width: '100%'
        }
    })