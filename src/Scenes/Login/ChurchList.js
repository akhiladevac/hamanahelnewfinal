import React, { Component } from "react";
import { View, Text, TouchableOpacity, Image, FlatList, NetInfo, TextInput, Alert,BackHandler} from "react-native";
import Icon from 'react-native-vector-icons/FontAwesome5';
import { Actions} from 'react-native-router-flux';
import AsyncStorage from '@react-native-community/async-storage';

const DATA_BASE_URL = '@base_url';
const BASE_URL = 'hamanahel.in/';

export default class ChurchList extends Component {

    constructor() {
        super();
  
        this.state = {
            loading: false,      
            data: [],
            temp: [],      
            error: null, 
            connection_Status:''
               
          };
  
        this.arrayholder = [];
    }
  
    componentDidMount() {
      console.log("Church List");
      this.backHandler = BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);     


      this.checkNetwork();
        this.getChurchList();
    }
    ShowAlertDialog=() =>{
 
      Alert.alert(
      
        'Internet Connection',
      'You are offline',
        [
         
     
          // Second Cancel Button in Alert Dialog.
          {text: 'Cancel', onPress: () => console.log('Cancel Button Pressed'), style: 'cancel'},
     
          // Third OK Button in Alert Dialog
          {text: 'retry',  onPress: () => this.checkNetwork() },
          
        ],
        {cancelable: false},
     
      )
     
    }
    checkNetwork=() => {
      NetInfo.isConnected.addEventListener(
        'connectionChange',
        this._handleConnectivityChange
  
    );
   
    NetInfo.isConnected.fetch().done((isConnected) => {
  
      if(isConnected == true)
      {
        this.setState({connection_Status : "Online"})
      }
      else
      {
        this.setState({connection_Status : "Offline"})
  
        this.ShowAlertDialog()      
  
  
      }
  
    });
  
    }
   
      
      handleBackPress = () => {
       
        if (Actions.currentScene !== 'Churchlist') {
        } else {
          Actions.ChooseChurch();
      
          return true;
        }
      }
    componentWillUnmount() {
      this.backHandler.remove();

      NetInfo.isConnected.removeEventListener(
          'connectionChange',
          this._handleConnectivityChange
   
      );
   
    }
   
    _handleConnectivityChange = (isConnected) => {
   
      if(isConnected == true)
        {
          this.setState({connection_Status : "Online"})
  
        }
        else
        {
          this.setState({connection_Status : "Offline"})
          this.ShowAlertDialog()      
  
          
  
  
        }
    };
   
  
  
    getChurchList() {
        this.setState({ loading: true });
          if (this.props.churchList !== null) {
            const abc = this.props.churchList;
          this.setState({          
              data: abc,  
              temp: abc,        
              error: null,          
              loading: false,
              query: '',        
            });  
            this.arrayholder = abc; 
            //console.log(this.state.data)
            } 
        } 
  
    searchFunction(query) {
        
        if (query.length === 0) {
            this.setState({ data: this.state.temp, query: query });
        } else {
            this.setState({ query: query, });
            this.searchFilterFunction(this.state.query);
        }
        
    }
  
    searchFilterFunction = text => {    
        const newData = this.arrayholder.filter(item => {      
          const itemData = `${item.church_name.toUpperCase()}   
                            ${item.place.toUpperCase()}`; 
          
          const textData = text.toUpperCase();
            
          return itemData.indexOf(textData) > -1;    
        });
        
        this.setState({ data: newData });  
      };
  
      selectChurchDomain(item) {
        this.storeBaseurl(item);
      }
  
      storeBaseurl = async (item) => {
        //var baseUrl = 'https://zero3.xyz/church/' + item.domain + '/'; 

        var baseUrl = 'https://' +item.domain + '.' + BASE_URL; 
        
        try {
          await AsyncStorage.setItem(DATA_BASE_URL, baseUrl);
          Actions.Login({ church_details: item });
          
        } catch (error) {
            console.error('Something went Wrong in Get Section' + error);
        }
      };
      image(item){
        if (item.image) {
          var profPic =  { uri: 'https://hamanahel.in/admin/' + item.image };
          return(
               <Image 
                  style={{ width: 50, height: 50, marginLeft: 10,borderRadius:20 }}
                  source={profPic}
                  />
          )

      } else {
        return(
          <View style={{ width: 50, height: 50, marginLeft: 10, borderRadius: 50, backgroundColor: '#7BCFE8' }}>
  </View>
        )
          }

      }
  
      renderProperties = ({ item }) => {
        console.log(item)
      //   if (item.image) {
      //     var profPic =  { uri: 'https://hamanahel.in/admin/' + item.image };

      // } else {
      //         var profPic = require('/home/linux/Music/sahad_ezed-test_hamanahel-fbe83dbb0a4e/android/app/src/main/res/drawable/logo.png');
      //     }
        return (
          <TouchableOpacity onPress={() => this.selectChurchDomain(item)}>
            <View style={{ backgroundColor: '#fff', height: 70, marginTop: 10, marginLeft: 10, marginRight: 10, borderRadius: 10 }}>
              <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center' }}>
                  {/* <Image 
                  style={{ width: 50, height: 50, marginLeft: 10,borderRadius:20 }}
                  source={profPic}
                  /> */}
                  
                {this.image(item)}
                  <View>
                      <Text style={{ paddingLeft: 15, fontSize: 18, fontWeight: '500' }}>{ item.church_name }</Text>
                      <Text style={{ paddingLeft: 15, paddingTop: 8 }}>{ item.place }</Text>
                  </View>
                
              </View>
            </View>
          </TouchableOpacity>
        );
      };
  
    render() {
        return (
            <View style={{ borderTopWidth: 0, borderBottomWidth: 0 }}>
            
                <View style={{ backgroundColor: '#7BCFE8'}}>
                <View style={{ flexDirection: 'row', alignItems: 'center', paddingLeft: 10, backgroundColor: '#fff', height: 40, margin: 20, marginTop: 30, borderRadius: 50, flexDirection: 'row' }}>
                    <TextInput
                      style={{ width: '90%', fontSize: 16, color: '#000' }} 
                      placeholder='Search'
                      onChangeText={text => this.searchFunction(text)}
                      autoCorrect={false}
                      // onChangeText={(query) => this.setState({ search: query })}
                    />
                    <Icon style={{ color: '#000'}} name={'search'} size={20} />
                </View>
                </View>
                <FlatList   
                    style={{ marginTop: 2 }}       
                    data={this.state.data}          
                    renderItem={this.renderProperties}          
                    keyExtractor={(item, index) => index.toString()}  
                    ItemSeparatorComponent={this.renderSeparator}                             
                />            
            </View>
        );
    }
}