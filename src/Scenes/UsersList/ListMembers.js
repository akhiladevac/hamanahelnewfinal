/* eslint-disable prettier/prettier */
import React, { Component } from 'react';
import { View, Text, FlatList, TouchableOpacity, Image, TextInput } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import AsyncStorage from '@react-native-community/async-storage';
import { Actions } from 'react-native-router-flux';

const DATA_LIST_MEMBERS = '@members_list';
const DATA_BASE_URL = '@base_url';
const LOGIN_CREDENTIALS = '@login_credentials';

export default class ListMembers extends Component {

    constructor() {
        super();

        this.state = {
            loading: false,      
            data: [],
            temp: [],      
            error: null,    
            baseUrl:'',
            abd:[]
          };

        this.arrayholder = [];
    }

    componentDidMount() {
      console.log("list of members");

      this.getMembersList();
      this.getUpdatedMembers();
    
     
    }

    async getMembersList() {
      this.setState({ loading: true });
      try {
          const membersList = await AsyncStorage.getItem(DATA_LIST_MEMBERS);
          if (membersList !== null) {
            const abc = JSON.parse(membersList);
          
          this.setState({          
              data: abc,  
              temp: abc,        
              error: null,          
              loading: false,
              query: '',        
            });  
            this.arrayholder = abc; 
            // alert(JSON.stringify(this.state.data))
          } 
      } catch (error) {
          console.log('Something went Wrong Saving list');
      }
  }

    getUpdatedMembers(){
      this.getBaseUrl();
    }

    async getBaseUrl() {
      try {
          let baseUrl = await AsyncStorage.getItem(DATA_BASE_URL);
          this.setState({ baseUrl: baseUrl });
          var loginCredentials = await AsyncStorage.getItem(LOGIN_CREDENTIALS);
          if (loginCredentials) {
            var login_credentials = JSON.parse(loginCredentials);
            //this.setState({ baseUrl: baseUrl, fam_id: login_credentials.family_id})
            var fam_id = login_credentials.family_id;
            this.UdtMemberList(baseUrl,fam_id);
          }
          
          
      } catch (error) {
          console.error('Something went Wrong in Get Section' + error);
      }
  }
  
      UdtMemberList(baseUrl,fam_id) {
          var endpoint = 'list_of_members.php';
          var url = baseUrl + endpoint;
          // console.log(url);
          var data = new FormData();
          data.append('hashcode', '##church00@');
          data.append('Fam_Id',fam_id)
      
          fetch(url, {
          method: 'POST',
          headers: {
              Accept: 'application/json',
              'Content-Type': 'multipart/form-data',
          },
          body: data,
          })
        .then(response => response.json())
        .then((responseJson)=> {

          if (responseJson.status === 200) {
            this.setState({
              loading: false,
              abd: responseJson.response
            });

            this.chechVariations();
            // console.log('sasasnmabsasb');
            // console.log(responseJson.response);

          } 
        })
        .catch(error=>console.log(error)) //to catch the errors if any
      }

      async chechVariations() {
        try {
            const membersList = await AsyncStorage.getItem(DATA_LIST_MEMBERS);
            if (membersList !== null) {
              const abc = JSON.parse(membersList);
            
              this.setState({          
                  data: abc,  
                  temp: abc,        
                  error: null,          
                  loading: false,
                  query: '',        
                });  
                this.arrayholder = abc; 
              } 

              if(JSON.stringify(this.state.abd) == JSON.stringify(this.state.data))
              { }else{
                this.updateAsyncValues(this.state.abd,DATA_LIST_MEMBERS);
              }
          
        } catch (error) {
            console.log('Something went Wrong Saving list');
        }
    }

    async updateAsyncValues(response, KEY) {
      await AsyncStorage.setItem(KEY, JSON.stringify(response));
      this.getMembersList();
    }

    searchFunction(query) {
        
        if (query.length === 0) {
            this.setState({ data: this.state.temp, query: query });
        } else {
            this.setState({ query: query, });
            this.searchFilterFunction(this.state.query);
        }
        
    }

    searchFilterFunction = text => {    
        const newData = this.arrayholder.filter(item => {      
          const itemData = `${item.name.toUpperCase()}   
                            ${item.houseName.toUpperCase()}`; 
          
           const textData = text.toUpperCase();
            
           return itemData.indexOf(textData) > -1;    
        });
        
        this.setState({ data: newData });  
      };

      renderProperties = ({ item }) => {
        //console.log(item.members);
        if (item.members[0].photo) {
          var profPic =  { uri: this.state.baseUrl + item.members[0].photo };
      } else {
              var profPic = require('../../img/avatar.png');
          }
        return (
          <TouchableOpacity onPress={() => Actions.DetailedUser({ fam_details: item, baseUrl: this.state.baseUrl })}>
            <View style={{ backgroundColor: '#fff', height: 70, marginTop: 10, marginLeft: 10, marginRight: 10, borderRadius: 10 }}>
              <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center' }}>
                  <Image 
                  style={{ width: 50, height: 50, marginLeft: 10, borderRadius: 20 }}
                  source={profPic}
                  />
                  <View>
                      <Text style={{ paddingLeft: 15, fontSize: 18, fontWeight: '500' }}>{ item.Fam_Owner }</Text>
                      <Text style={{ paddingLeft: 15, paddingTop: 8 }}>{ item.Fam_Name } House</Text>
                  </View>
                
              </View>
            </View>
          </TouchableOpacity>
        );
      };

    render() {
        return (
            <View style={{ borderTopWidth: 0, borderBottomWidth: 0 }}>

                <FlatList   
                    style={{ marginTop: 2 }}       
                    data={this.state.data}          
                    renderItem={this.renderProperties}          
                    keyExtractor={(item, index) => index.toString()}  
                    ItemSeparatorComponent={this.renderSeparator}                             
                />            
            </View>
        );
    }
}