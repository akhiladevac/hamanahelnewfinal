/* eslint-disable prettier/prettier */
/* eslint-disable no-trailing-spaces */
import React, { Component } from 'react';
import { View, Text, FlatList, TouchableOpacity,Alert, Image,Linking, TextInput,BackHandler,Picker,location } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import AsyncStorage from '@react-native-community/async-storage';
import { Actions } from 'react-native-router-flux';
const DATA_BASE_URL = '@base_url';
const LOGIN_CREDENTIALS = '@login_credentials';
const DATA_LIST_MEMBERS = '@members_list';
import Dialog, { DialogTitle, DialogFooter, DialogButton, DialogContent } from 'react-native-popup-dialog';


export default class Bloodlist extends Component {

    constructor() {
        super();

        this.state = {
            loading: false,      
            // data: [],
            temp: [],  
            baseUrl: '',    
            error: null,  
            abd: []  ,
            familycount:'',
            someVal:'',
            visiblelist:false,
             PickerSelectedVal:'',
             throttlemode:'',
             choosenLabel: '', choosenindex: '',
             category:'',
             currentLabel: '',
             response3: [
              { 'bloodgroup': 'A+'},
              { 'bloodgroup': 'B+'},
              { 'bloodgroup': 'O+'},
             
             ],
             currency:'',
             response3:[
               {bloodgroup:'A+'},
               {bloodgroup:'A-'},
               {bloodgroup:'B+'},
               {bloodgroup:'B-'},
               {bloodgroup:'O+'},
               {bloodgroup:'O-'},
               {bloodgroup:'AB+'},
               {bloodgroup:'AB-'},
               {bloodgroup:'AB-'},



            
            ],
            visibleEmail: false,
            visibleOtp: false,
            visibleNewPassword: false,
  
            passwordRe: '',
            passwordReCon: '',
  
           
          };
        
        this.arrayholder = [];
    }

    static onEnterSomeView = () => {
      Actions.refresh();
    }
    // componentWillMount(){
    //   this.setState({visibleEmail:true});




    // }
    componentDidMount() {
      // location.reload();      // this.setState({visibleEmail:true});

        this.getBaseUrl();
        // this.backHandler = BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);     


    }
    // componentWillUnmount() {
    //   this.backHandler.remove();
    // }
   
   
   async getBaseUrl() {
      try {
          let baseUrl = await AsyncStorage.getItem(DATA_BASE_URL);
          this.setState({ baseUrl: baseUrl });
           this.setState({visibleEmail:true});

            // this.bloodListMembers(baseUrl,this.state.throttlemode);
          
      } catch (error) {
          console.error('Something went Wrong in Get Section' + error);
      }
  }

  
    
    
      renderProperties = ({ item }) => {
          
        if (item.photo) {
          var profPic =  { uri: this.state.baseUrl + item.photo };
      } else {
              var profPic = require('../../img/avatar.png');
          }
          if (item.phone && item.phone !== '0') {
            var phone = item.phone;
        } else {
            var phone = '';
        }
        return (
          <TouchableOpacity onPress={() => Actions.MemberSearch({ bloodData: item.bloodgroup})}>
              
            <View style={{ backgroundColor: '#fff', height: 55, marginTop: 10, marginLeft: 10, marginRight: 10, borderRadius: 10 }}>
              <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center' }}>
                
                  <View style={{ marginLeft: 10, width: '50%'}} >
                
                <Text style={{ fontSize: 17, fontWeight: '500',marginLeft:35 }}>{item.bloodgroup}</Text>
                {/* <Text style={{ fontSize: 10, paddingLeft: 2, paddingTop: 5 }}>{item.Fam_Name}</Text>
                <View style={{flexDirection:'row'}}>
                <Text style={{ fontSize: 10, paddingLeft: 2, paddingTop: 5 }}>Family Owner :</Text>

                <Text style={{ fontSize: 10, paddingLeft: 2, paddingTop: 5 }}>{item.Fam_Owner}</Text> */}
                {/* </View> */}
        
        </View>
   
              </View>
            </View>
          </TouchableOpacity>
        );
      };
      
  renderContents() {

    
    
        return (
              <FlatList   
                style={{ marginTop: 5,}}       
                data={this.state.response3}          
                renderItem={this.renderProperties}          
                keyExtractor={(item, index) => index.toString()}  
                ItemSeparatorComponent={this.renderSeparator}  
                                          
            />       
        )
    
}



    render() {
        return (
            <View style={{ borderTopWidth: 0, borderBottomWidth: 0 }}>
            
                <View style={{ backgroundColor: '#7BCFE8'}}>
                <View style={{ flexDirection: 'row', alignItems: 'center', paddingLeft: 10, backgroundColor: '#7BCFE8', height: 80, flexDirection: 'row' }}>
                <Text style={{ fontSize: 22, fontWeight: 'bold',marginLeft:10 }}>Select a Blood Group</Text>

                </View>
                </View>
                {this.renderContents()}
                {/* <FlatList   
                style={{ marginTop: 9,}}       
                data={this.state.eventsData}          
                renderItem={this.renderItem}          
                keyExtractor={(item, index) => index.toString()}  
                ItemSeparatorComponent={this.renderSeparator}  
                                          
            />        */}
            </View>
        );
    }
}