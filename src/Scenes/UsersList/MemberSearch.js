/* eslint-disable prettier/prettier */
/* eslint-disable no-trailing-spaces */
import React, { Component } from 'react';
import { View, Text, FlatList, TouchableOpacity,Alert, Image,Linking,ActivityIndicator, TextInput,BackHandler,Picker,location } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import AsyncStorage from '@react-native-community/async-storage';
import { Actions } from 'react-native-router-flux';
const DATA_BASE_URL = '@base_url';
const LOGIN_CREDENTIALS = '@login_credentials';
const DATA_LIST_MEMBERS = '@members_list';
import Dialog, { DialogTitle, DialogFooter, DialogButton, DialogContent,DialogActions } from 'react-native-popup-dialog';


export default class MemberSearch extends Component {

    constructor() {
        super();

        this.state = {
            loading: false,      
            // data: [],
            temp: [],  
            baseUrl: '',    
            error: null,  
            abd: []  ,
            familycount:'',
            someVal:'',
            visiblelist:false,
             PickerSelectedVal:'',
             throttlemode:'',
             choosenLabel: '', choosenindex: '',
             category:'',
             currentLabel: '',
             response3: [
              { 'bloodgroup': 'A+'},
              { 'bloodgroup': 'B+'},
              { 'bloodgroup': 'O+'},
             
             ],
             currency:'',
             response3:[
               {bloodgroup:'A+'},
               {bloodgroup:'A-'},
               {bloodgroup:'B+'},
               {bloodgroup:'B-'},
               {bloodgroup:'O+'},
               {bloodgroup:'O-'},
               {bloodgroup:'AB+'},
               {bloodgroup:'AB-'},

            
            ],
            visibleEmail: false,
            visibleOtp: false,
            visibleNewPassword: false,
  
            passwordRe: '',
            passwordReCon: '',
  
           
          };
        
        this.arrayholder = [];
    }

    static onEnterSomeView = () => {
      Actions.refresh();
    }
    // componentWillMount(){
    //   this.setState({visibleEmail:true});


    // }
    componentDidUpdate(prevProps, prevState) {
      if(prevProps.visibleEmail== false){
        //Perform some operation here
        alert("dyd"+prevProps.visibleEmail);
        this.setState({visibleEmail: true});
      }
    }
    
    componentWillReceiveProps(nextProps){
      if(nextProps.visibleEmail!==true){
        //Perform some operation
        alert("receve"+nextProps.visibleEmail);

        this.setState({visibleEmail: true });
      }
    }
    // componentDidUpdate(params){
    //   this.setState({visibleEmail:true});


    // }
    componentDidMount() {
      // location.reload();      // this.setState({visibleEmail:true});

        this.getBaseUrl();
        this.backHandler = BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);     


    }
    componentWillUnmount() {
      this.backHandler.remove();
    }
   
    handleBackPress = () => {
     
      if (Actions.currentScene !== '_MemberSearch') {
          
      } else {
          this.setState({ visibleEmail: false })

        Actions.pop();
    
        return true;
      }
    }
   async getBaseUrl() {
      try {
          let baseUrl = await AsyncStorage.getItem(DATA_BASE_URL);
          this.setState({ baseUrl: baseUrl });
           this.setState({visibleEmail:true});

            // this.bloodListMembers(baseUrl,this.state.throttlemode);
          
      } catch (error) {
          console.error('Something went Wrong in Get Section' + error);
      }
  }

    // bloodListMembers(baseUrl,r){
    //   this.setState({
    //     abd:[],
    //     throttlemode:r
    //   })
    //   // alert(r)
    //   var endpoint = 'blood_group_list.php';
    //    var url = baseUrl + endpoint;
    //     // console.log(url);
    //     var data = new FormData();
    //     data.append('hashcode', '##church00@');
    //     data.append('blood_group',r);
     
    //     fetch(url, {
    //     method: 'POST',
    //     headers: {
    //         Accept: 'application/json',
    //         'Content-Type': 'multipart/form-data',
    //     },
    //     body: data,
    //     })
    //   .then(response => response.json())
    //   .then((responseJson)=> {

    //     if (responseJson.status === 200) {
    //       this.setState({
    //         loading: false,
    //         abd: responseJson.response,
        
    //          })
    //       console.log("jhj"+JSON.stringify(responseJson) )
       
    //     }else{
    //       Alert.alert('No Data Found')

    //     }
        
    //   })
    //   .catch(error=>console.log(error)) //to catch the errors if any
    //   }

    

    
      bloodListMembers1(baseUrl,r){
        this.setState({loading:true})
        this.setState({
          abd:[],
          PickerSelectedVal:r,
          visibleEmail:false,
          
        })
        // alert(r)
        var endpoint = 'blood_group_list.php';
         var url = baseUrl + endpoint;
          // console.log(url);
          var data = new FormData();
          data.append('hashcode', '##church00@');
          data.append('blood_group',r);
       
          fetch(url, {
          method: 'POST',
          headers: {
              Accept: 'application/json',
              'Content-Type': 'multipart/form-data',
          },
          body: data,
          })
        .then(response => response.json())
        .then((responseJson)=> {
  
          if (responseJson.status === 200) {
            this.setState({
              loading: false,
              abd: responseJson.response,
          
               })
            console.log("jhj"+JSON.stringify(responseJson) )
         
          }else{
            this.setState({
              loading: false,
             
          
               })
  
          }
          
        })
        .catch(error=>console.log(error)) //to catch the errors if any
        }
  
        renderProperties1 = ({ item }) => {
          
          if (item.photo) {
            var profPic =  { uri: this.state.baseUrl + item.photo };
        } else {
                var profPic = require('../../img/avatar.png');
            }
            if (item.phone && item.phone !== '0') {
              var phone = item.phone;
          } else {
              var phone = '';
          }
          return (
            <TouchableOpacity onPress={() => this.bloodListMembers1(this.state.baseUrl,item.bloodgroup)}>
                
              <View style={{ backgroundColor: '#fff', height: 40, marginTop: 5, marginLeft:5,marginRight:5 }}>
                <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center' }}>
                  
                    <View style={{ marginLeft: 10, width: '50%',alignItems: 'center',justifyContent:'center'}} >
                  
                  <Text style={{ fontSize: 17, fontWeight: '500',marginLeft:85 }}>{item.bloodgroup}</Text>
                  {/* <Text style={{ fontSize: 10, paddingLeft: 2, paddingTop: 5 }}>{item.Fam_Name}</Text>
                  <View style={{flexDirection:'row'}}>
                  <Text style={{ fontSize: 10, paddingLeft: 2, paddingTop: 5 }}>Family Owner :</Text>
  
                  <Text style={{ fontSize: 10, paddingLeft: 2, paddingTop: 5 }}>{item.Fam_Owner}</Text> */}
                  {/* </View> */}
          
          </View>
     
                </View>
              </View>
            </TouchableOpacity>
          );
        };
        
      renderProperties = ({ item }) => {
          
        if (item.photo) {
          var profPic =  { uri: this.state.baseUrl + item.photo };
      } else {
              var profPic = require('../../img/avatar.png');
          }
          if (item.phone && item.phone !== '0') {
            var phone = item.phone;
        } else {
            var phone = '';
        }
        return (
          <TouchableOpacity >
            <View style={{ backgroundColor: '#fff', height: 85, marginTop: 10, marginLeft: 10, marginRight: 10, borderRadius: 10 }}>
              <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center' }}>
                  <Image 
                  style={{ width: 50, height: 50, marginLeft: 10, borderRadius: 20 }}
                  source={profPic}
                  />
                  <View style={{ marginLeft: 10, width: '50%'}} >
                
                <Text style={{ fontSize: 17, fontWeight: '500' }}>{item.Person_Name}</Text>
                <Text style={{ fontSize: 10, paddingLeft: 2, paddingTop: 5 }}>{item.Fam_Name}</Text>
                <View style={{flexDirection:'row'}}>
                <Text style={{ fontSize: 10, paddingLeft: 2, paddingTop: 5 }}>Family Owner :</Text>

                <Text style={{ fontSize: 10, paddingLeft: 2, paddingTop: 5 }}>{item.Fam_Owner}</Text>
                </View>
        
        </View>
   <View style={{flexDirection:'row',marginLeft:60}}>

        {this.renderCall(phone)}
        </View>
                
              </View>
            </View>
          </TouchableOpacity>
        );
      };
      renderCall(phone) {
        if (phone) {
            return (
                <TouchableOpacity onPress={() => this.callFuntion(phone)}>
                    <Icon style={{marginLeft:8}}   name="phone" size={20} color="#000" />
                </TouchableOpacity>
            )
        }
    }
    callFuntion(number) {
  

      if (number !== null && number.length >= 8) {
        Linking.openURL(`tel:${number}`);
      } else {
        Alert.alert('Number is Empty');
      }
  }
  renderPhoneCheck() {
    return (
        <View>
        <Dialog
            visible={this.state.visibleEmail}
            dialogTitle={<DialogTitle title=" Select a Blood Group" />}
            footer={
                <DialogFooter>
                  
                    <DialogButton 
                     text="Close"
                    onPress={() => { this.phoneSubmitBtn() }}
                    />
                </DialogFooter>
                }
            >
            <DialogContent>

                <View style={{ width: 250, alignItems: 'center' }}>

                    <View style={{ marginTop: 20, width: 250, height: 300, backgroundColor: '#cfcdcc', borderRadius: 5,flexDirection:'row' }}>
                    <FlatList   
        style={{ marginTop: 2 }}       
        data={this.state.response3}          
        renderItem={this.renderProperties1}          
        keyExtractor={(item, index) => index.toString()}  
        ItemSeparatorComponent={this.renderSeparator}                             
    />    
     
                    {/* <Text style={{width:100,height:40,marginTop:10,marginLeft:20,fontWeight:'700'}}>Blood Group :</Text>

                        <Text style={{width:60,height:40,marginTop:10,marginLeft:10}}>{this.state.PickerSelectedVal}</Text>
                       
                        <Picker style={{width: '8%',height:40,marginBottom:20}}
    selectedValue={this.state.PickerSelectedVal}
    onValueChange={(itemValue, itemIndex) => 
      this.bloodListMembers1(this.state.baseUrl,itemValue)}>
    {/* onValueChange={(itemValue, itemIndex) => this.setState({PickerSelectedVal: itemValue})} > */}
    {/* <Picker.Item label="Select a blood group" value="Select a blood group" />

    <Picker.Item label="A+" value="A+" />
    <Picker.Item label="A-" value="A-" />
    <Picker.Item label="B+" value="B+" />
    <Picker.Item label="B-" value="B-" />
     <Picker.Item label="O+" value="O+" />
     <Picker.Item label="O-" value="O-" />
     <Picker.Item label="AB+" value="AB+" />
     <Picker.Item label="AB-" value="AB-" />



   </Picker> */}
 
                    </View>
                </View>

            </DialogContent>
        </Dialog>
    </View>
    )
}
renderContents() {
  if (this.state.loading) {
    return (
        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center',marginTop:100}}>
            <ActivityIndicator size='large' />
        </View>
    );
}else if(this.state.abd.length==0){
  return (
    <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
       <Text style={{marginTop:400}}>No Data Found </Text>
    </View>
  );



} 

  
  

      return (
        <FlatList   
        style={{ marginTop: 2 }}       
        data={this.state.abd}          
        renderItem={this.renderProperties}          
        keyExtractor={(item, index) => index.toString()}  
        ItemSeparatorComponent={this.renderSeparator}                             
    />    
      );
  
}
PhoneCheckForPassword(baseUrl){
  this.setState({
    abd:[],
    visibleEmail: false

  })
    // alert(this.state.PickerSelectedVal)
  var endpoint = 'blood_group_list.php';
   var url = baseUrl + endpoint;
    // console.log(url);
    var data = new FormData();
    data.append('hashcode', '##church00@');
    data.append('blood_group',this.state.PickerSelectedVal);
 
    fetch(url, {
    method: 'POST',
    headers: {
        Accept: 'application/json',
        'Content-Type': 'multipart/form-data',
    },
    body: data,
    })
  .then(response => response.json())
  .then((responseJson)=> {

    if (responseJson.status === 200) {
      this.setState({
        loading: false,
        abd: responseJson.response,
    
         })
      console.log("jhj"+JSON.stringify(responseJson) )
   
    }else{
      Alert.alert('No Data Found')

    }
    
  })
  .catch(error=>console.log(error)) //to catch the errors if any
  


}

phoneSubmitBtn() {
  this.setState({ visibleEmail: false })

  Actions.Home();
//   if(this.state.PickerSelectedVal){
//     this.PhoneCheckForPassword(this.state.baseUrl)

//   }else{
// Alert.alert('Please select a blood group')
//   }
  
 
}
    render() {
        return (
          <View style={{ borderTopWidth: 0, borderBottomWidth: 0 }}>
            
          <View style={{ backgroundColor: '#7BCFE8'}}>

          {this.renderPhoneCheck()}

          <View style={{marginTop:25,marginLeft:20}}>
                <Text style={{fontSize:22,fontWeight:'700',marginLeft:10}}>Blood Group Search</Text>

                </View> 
           

<View style={{justifyContent:'center',flexDirection:'row', alignItems:'center'}}>


           
            </View>
            
          <View style={{ flexDirection: 'row', alignItems: 'center', paddingLeft: 10, backgroundColor: '#fff', height: 40, margin: 20, marginTop: 20, borderRadius: 50, flexDirection: 'row' }}>
          {/* <Picker style={{width: '90%',height:50,marginBottom:20,marginTop:20}}
            selectedValue={this.state.category}
 
            onValueChange={(itemValue, itemIndex) => this.setState({category: itemValue})} >
 
            { this.state.response3.map((item, key)=>(
            <Picker.Item label={item.bloodgroup} value={item.bloodgroup} key={key} />)
            )}
    
          </Picker> */}
          <Text style={{width:'80%',marginLeft:10}} onPress={() => this.setState({visibleEmail:true})}>Select a Blood Group</Text>
          {/* <Picker style={{width: '80%',height:50}}
    selectedValue={this.state.throttlemode}
    onValueChange={(someVal, throttlemodeIndex) => 
    this.bloodListMembers(this.state.baseUrl,someVal)}>
    <Picker.Item label="Select a blood group" value="Select a blood group" />

    <Picker.Item label="A+" value="A+" />
    <Picker.Item label="A-" value="A-" />
    <Picker.Item label="B+" value="B+" />
    <Picker.Item label="B-" value="B-" />
     <Picker.Item label="O+" value="O+" />
     <Picker.Item label="O-" value="O-" />
     <Picker.Item label="AB+" value="AB+" />
     <Picker.Item label="AB-" value="AB-" />



   </Picker> */}

            
            <Icon  style={{ color: '#000'}} name={'search'} size={18} />

          </View>

          </View>
          
                      {this.renderContents()}
        
      </View>
  );
}
}        