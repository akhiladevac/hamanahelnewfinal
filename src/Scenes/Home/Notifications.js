/* eslint-disable prettier/prettier */
/* eslint-disable react-native/no-inline-styles */
import React, { Component } from 'react';
import { View, Text, ScrollView, Image, TouchableOpacity, ActivityIndicator,NetInfo,Alert } from 'react-native';
import { Actions } from 'react-native-router-flux';
import Icon from 'react-native-vector-icons/FontAwesome5';
import AsyncStorage from '@react-native-community/async-storage';

const DATA_BASE_URL = '@base_url';
const LOGIN_CREDENTIALS = '@login_credentials';

export default class Notifications extends Component {

    constructor() {
        super();
         this.viewUpdateNotifications = this.viewUpdateNotifications.bind(this)

        this.state = {
            response: [],
            response1: [],

            birthdayResponse: [],
            loadingStatus: true,
            connection_Status : '',
            settimeout:false,
            responseNotification:[],
            baseUrl: '',
            fam_id: '',
            userRole:'',
            deletenotifresponse:[],
            notification_id:'',
            responsenotifi:'',
            response2:[]
        };
    }
    
    componentDidMount() {
        console.log("notification");

        this.checkNetwork();
        this.getBaseUrl();

    }
    apiCallBirthday(){
            this.apiCallBirthdayPriest(this.state.baseUrl,this.state.fam_id);


        

    }
    async getBaseUrl(){

        try {
            let baseUrl = await AsyncStorage.getItem(DATA_BASE_URL);
            var loginCredentials = await AsyncStorage.getItem(LOGIN_CREDENTIALS);
            if (loginCredentials) {
             var login_credentials = JSON.parse(loginCredentials);
             var fam_id = login_credentials.family_id;
            this.setState({ userRole: login_credentials.role, baseUrl: baseUrl, fam_id: fam_id });
           
                this.apiCallBirthdayPriest(baseUrl,fam_id);

           
                this.apiCallBirthdayUser(baseUrl,fam_id);
            }
            
            
        } 
        catch(error){
            console.error('Something went wrong in Get Section '+ error)
        }
    }
    checkNetwork=() => {
        NetInfo.isConnected.addEventListener(
          'connectionChange',
          this._handleConnectivityChange
    
      );
     
      NetInfo.isConnected.fetch().done((isConnected) => {
    
        if(isConnected == true)
        {
          this.setState({connection_Status : "Online"})
        }
        else
        {
          this.setState({connection_Status : "Offline"})
    
    
    
        }
    
      });
    
      }
      componentWillUnmount() {
    
        NetInfo.isConnected.removeEventListener(
            'connectionChange',
            this._handleConnectivityChange
     
        );
     
      }
     
      _handleConnectivityChange = (isConnected) => {
     
        if(isConnected == true)
          {
            this.setState({connection_Status : "Online"})
    
          }
          else
          {
            this.setState({connection_Status : "Offline"})
    
            
    
    
          }
      };
     
    
    
     showAlert(){
        Alert.alert(  
            'You are in offline',  
            [  
                {  
                    text: 'Cancel',  
                    onPress: () => console.log('Cancel Pressed'),  
                    style: 'cancel',  
                },  
                {text: 'Retry', onPress: () => this.checkNetwork()},  
            ]  
        );  
     }
    
     componentWillMount(){

        NetInfo.isConnected.removeEventListener(
            'connectionChange',
            this._handleConnectivityChange
     
        );
     
      }
     
      _handleConnectivityChange = (isConnected) => {
     
        if(isConnected == true)
          {
            this.setState({connection_Status : "Online"})
          }
          else
          {
            this.setState({connection_Status : "Offline"})
            return (
                // <TouchableOpacity  onPress={() => Actions.detailedNotification({ contents: response, title: 'Notifications' })}>
                    <View style={{ margin: 5, borderRadius: 5, height: 50, flexDirection: 'row', alignItems: 'center', backgroundColor: '#000' }}>
                       
                        <Text style={{ color: '#fff', fontSize: 11, fontWeight: 'bold', width: 100, paddingLeft: 10, paddingRight: 10 }}> You are offline </Text>
                        
                    </View>
                   
            );
          }
      };
  
    viewUpdateNotifications(){
        this.getBaseUrl();
      }
    async apiCall(baseUrl) {
        this.setState({ loadingStatus: true, response: [] });

        var endpoint = 'notification.php';
        var url = baseUrl + endpoint;
        // console.log(url);
        var data = new FormData();
        data.append('hashcode', '##church00@');
        fetch(url, {
        method: 'POST',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'multipart/form-data',
        },
        body: data,
        })
       

        .then((response) => response.json())
        
        .then((responseJson) => {
            // console.log(responseJson);
            this.setState({ 
                 response: this.state.response.concat(responseJson.response), 
            //  response: responseJson.response, 

                loadingStatus: false,
                  responseNotification:this.state.response.concat(responseJson.response),
            })
            // alert(responseJson.response.msg)

            // console.log(responseJson);
        }) 
        
        .catch((error) => {
          console.error(error);
        });
        // setTimeout(() => abort, 1000)
    }
   
    async apiCallBirthdayPriest(baseUrl, fam_id) {

                

        this.setState({ loadingStatus: true, response: [] });
        var endpoint = 'wish_notification.php';
        var url = baseUrl + endpoint;
        // console.log(url);
        var data = new FormData();
        data.append('hashcode', '##church00@');
        data.append('Fam_Id', fam_id);
        fetch(url, {
        method: 'POST',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'multipart/form-data',
        },
        body: data,
        })
        .then((response) => response.json())
        .then((responseJson) => {
            console.log('bithday');
            // console.log(responseJson);
            if (responseJson.status === 200) {
                this.setState({ 
                    response: responseJson.response,
                    birthdayResponse: responseJson.response,
                    settimeout:'false'
                });
                this.apiCall(baseUrl);

            } else {
                this.apiCall(baseUrl);


            }
            // console.log(responseJson);
        })
        .catch((error) => {
          console.error(error);
        });
        setTimeout(() => {
            this.setState({
                loadingStatus:false,
                settimeout:true
            })
            },45000)

    }
    async apiCallBirthdayUser(baseUrl, fam_id) {

        this.setState({ loadingStatus: true, response: [] });
        var endpoint = 'wish_notification.php';
        var url = baseUrl + endpoint;
        // console.log(url);
        var data = new FormData();
        data.append('hashcode', '##church00@');
        data.append('Fam_Id', fam_id);
        fetch(url, {
        method: 'POST',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'multipart/form-data',
        },
        body: data,
        })
        .then((response) => response.json())
        .then((responseJson) => {
            console.log('bithday');
            // console.log(responseJson);
            if (responseJson.status === 200) {
                this.setState({ 
                    response1: responseJson.response,
                    birthdayResponse: responseJson.response,
                    settimeout:'false'
                });
                this.notificationCategory(baseUrl,this.state.fam_id)

                this.allNotification(baseUrl);
            } else {
                this.notificationCategory(baseUrl,this.state.fam_id)

                this.allNotification(baseUrl);
            }
            // console.log(responseJson);
        })
        .catch((error) => {
          console.error(error);
        });
       

    }
    async allNotification(baseUrl) {

        this.setState({ loadingStatus: true, });
        var endpoint = 'all_notification.php';
        var url = baseUrl + endpoint;
        // console.log(url);
        var data = new FormData();
        data.append('hashcode', '##church00@');
        fetch(url, {
        method: 'POST',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'multipart/form-data',
        },
        body: data,
        })
        .then((response) => response.json())
        .then((responseJson) => {
            console.log('bithday');
            // console.log(responseJson);
            if (responseJson.status === 200) {
                this.setState({ 
                    response1: this.state.response1.concat(responseJson.response), 
                    loadingStatus: false,
                    
                   
                });
                //   this.notificationCategory(baseUrl,this.state.fam_id)
            } else {
                this.setState({ 
                    response1: this.state.response1, 
                    loadingStatus: false,
                    
                   
                });
                //    this.notificationCategory(baseUrl,this.state.fam_id)

            }
             console.log("allnotif"+JSON.stringify(responseJson) );
        })
        .catch((error) => {
          console.error(error);
        });
        setTimeout(() => {
            this.setState({
                loadingStatus:false,
                settimeout:true
            })
            },45000)
       

    }
    async notificationCategory(baseUrl, fam_id) {

        this.setState({ loadingStatus: true, });
        var endpoint = 'notification_category.php';
        var url = baseUrl + endpoint;
        // console.log(url);
        var data = new FormData();
        data.append('hashcode', '##church00@');
        data.append('Fam_Id', fam_id);
        fetch(url, {
        method: 'POST',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'multipart/form-data',
        },
        body: data,
        })
        .then((response) => response.json())
        .then((responseJson) => {
            console.log('bithday');
            // console.log(responseJson);
            if (responseJson.status === 200) {
                this.setState({ 
                    response1: this.state.response1.concat(responseJson.response), 
                   
                });
                
            } else {
               
            }
             console.log("gdhtdft"+JSON.stringify(responseJson) );
        })
        .catch((error) => {
          console.error(error);
        });
        
    }
    renderNotifications() {
       if(this.props.role=='priest'){

        return (
            this.state.response.map((response) => {

            if (response.date) {
                var months = [
                    'January',
                    'February',
                    'March',
                    'April',
                    'May',
                    'June',
                    'July',
                    'August',
                    'September',
                    'October',
                    'November',
                    'December'
                  ];
                
                  var Value = response.date.split("-");
                  const monthVar = Number.parseInt(Value[1]);
                  var date = Value[2];
                  var month = months[monthVar - 1]; 
            } else {
                var date = '';
                var month = '';
            }

            // if (response.photo) {
            //     var profPic =  { uri: this.state.baseUrl + response.photo };
            // } else {
                    var profPic = require('../../img/man.png');
                // }
                return (
                    <TouchableOpacity key={response.id} 
                     onPress={() => Actions.detailedNotification({ contents: response, title: 'Notifications' })}>
                    <View key={response.id} style={{ height: 200, width: 160, backgroundColor: '#fff', borderRadius: 10, marginLeft: 12,}}>
                        <View style={{ margin: 5, borderRadius: 5, height: 50, flexDirection: 'row', alignItems: 'center', backgroundColor: '#000' }}>
                            {/* <Image 
                                style={{ height: 40, width: 40, borderRadius: 20, marginLeft: 5,  }}
                                source={profPic}
                            />
                            <Text style={{ color: '#fff', fontSize: 11, fontWeight: 'bold', width: 80, paddingLeft: 1, paddingRight: 10 }}> {response.role} </Text> */}
  <Text style={{ color: '#fff', fontSize: 11, fontWeight: 'bold', width: 80, paddingLeft: 7, paddingRight: 10 }}> Priest </Text> 

                            </View>
                        <Text style={{ color: '#000', marginTop: 5, textAlign: 'right', paddingRight: 20, fontSize: 9, }}> {date} {month}</Text>

                        <Text style={{ fontSize: 13, fontWeight: '600', marginTop: 10, paddingLeft: 10, paddingRight: 10}}> {response.title}</Text>

                    </View>
                    </TouchableOpacity>
                );

                    
            })
        );
    }
    else if(this.props.role=='user'){
        return (
            this.state.response1.map((response1) => {

            if (response1.date) {
                var months = [
                    'January',
                    'February',
                    'March',
                    'April',
                    'May',
                    'June',
                    'July',
                    'August',
                    'September',
                    'October',
                    'November',
                    'December'
                  ];
                
                  var Value = response1.date.split("-");
                  const monthVar = Number.parseInt(Value[1]);
                  var date = Value[2];
                  var month = months[monthVar - 1]; 
            } else {
                var date = '';
                var month = '';
            }

            // if (response1.photo) {
            //     var profPic =  { uri: this.state.baseUrl + response1.photo };
            // } else {
                    var profPic = require('../../img/man.png');
                // }
                return (
                    <TouchableOpacity key={response1.id} 
                     onPress={() => Actions.detailedNotification({ contents: response1, title: '' })}>
                    <View key={response1.id} style={{ height: 200, width: 160, backgroundColor: '#fff', borderRadius: 10, marginLeft: 12,}}>
                        <View style={{ margin: 5, borderRadius: 5, height: 50, flexDirection: 'row', alignItems: 'center', backgroundColor: '#000' }}>
                            {/* <Image 
                                style={{ height: 40, width: 40, borderRadius: 20, marginLeft: 5,  }}
                                source={profPic}
                            /> */}
                            {/* <Text style={{ color: '#fff', fontSize: 11, fontWeight: 'bold', width: 80, paddingLeft: 1, paddingRight: 10 }}> {response1.role} </Text> */}
                            <Text style={{ color: '#fff', fontSize: 11, fontWeight: 'bold', width: 80, paddingLeft: 7, paddingRight: 10 }}> Priest </Text> 

                            </View>
                        <Text style={{ color: '#000', marginTop: 5, textAlign: 'right', paddingRight: 20, fontSize: 9, }}> {date} {month}</Text>

                        <Text style={{ fontSize: 13, fontWeight: '600', marginTop: 10, paddingLeft: 10, paddingRight: 10}}> {response1.title}</Text>

                    </View>
                    </TouchableOpacity>
                );

                    
            })
        );
        console.log("state"+this.state.response1)
    }
               
    
}

    renderContents() {

        if (this.state.loadingStatus) {
            return (
                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center'}}>
                    <ActivityIndicator size='large' />
                </View>
            );
        // }else if(this.state.response.length == 0)  {
        //     return (
        //         <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center'}}>
        //             <Text style={{fontSize:18}}>No New Notifications</Text>
        //     </View>
        //     )
         } 
        else if(this.state.loadingStatus==false && this.state.settimeout == true && this.state.connection_Status=='Offline' ){

            return (
                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center'}}>
                    <Text style={{fontSize:18}}>You are offline</Text>
                    <View style={{ flexDirection: 'row', alignItems: 'center'}}>
                    <TouchableOpacity onPress={() => this.apiCallBirthday(this.state.baseUrl, this.state.fam_id)}>
                        <Icon style={{ marginTop:20}} name="redo" size={17} color="#fff" />
                        </TouchableOpacity>
                    <Text style={{fontSize:18,marginTop:20,marginLeft:10}}>Retry</Text>
                    </View>

                </View>
            );
        }
        else if(this.state.connection_Status=='Online')  {
            return (
                <ScrollView horizontal showsHorizontalScrollIndicator={false} style={{ flex: 1, marginTop: 30, }}>
                    
                    {this.renderNotifications()}
                </ScrollView>
            )
        } 
        else{
            // return (
            //     <ScrollView horizontal showsHorizontalScrollIndicator={false} style={{ flex: 1, marginTop: 30, }}>
                    
            //         {this.renderNotifications()}
            //     </ScrollView>
            // )

        }
    }

    render() {
        return (
            
            <View style={{ flex: 1 }}>
                 {/* <View style={{ position: 'absolute', marginLeft: '72%', marginTop: -20, marginBottom: 20 }}> */}
                    <View style={{ position: 'absolute', marginLeft: '75%'}}>
                       <TouchableOpacity onPress={() => this.apiCallBirthday()}><Icon style={{ marginLeft: '40%' }} name="redo" size={17} color="#fff" /></TouchableOpacity>
                    </View>
            {this.renderContents()}
            
            </View>
        );
    }
}