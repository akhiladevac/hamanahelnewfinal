/* eslint-disable prettier/prettier */
import React, { Component } from 'react';
import { View, Text, ScrollView, ActivityIndicator, TouchableOpacity, Dimensions, Alert, BackHandler, ToastAndroid } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome5';
import Notifications from './Notifications';
import ListMembers from '../../Scenes/UsersList/ListMembers';
import AsyncStorage from '@react-native-community/async-storage';
import { Actions } from 'react-native-router-flux';

const DATA_LIST_MEMBERS = '@members_list';
const DATA_BASE_URL = '@base_url';
const LOGIN_CREDENTIALS = '@login_credentials';
const MAX_HEIGHT = Dimensions.get('window').height;
const MAX_WIDTH = Dimensions.get('window').width;

export default class Home extends Component {

    constructor() {
        super();

        this.state = {
            loadingStatus: false,
            userRole: '',
            fam_id: '',
            loadingStatusNotify: false,
            baseUrl: ''
        }
    }

    componentDidMount() {
        console.log("home");
        this.checkUserRole();   
        this.backHandler = BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);     
    }
    
      componentWillUnmount() {
        this.backHandler.remove();
      }
    
      handleBackPress = () => {
        if (Actions.currentScene !== '_Home') {
        } else {
            ToastAndroid.show('Press again to exit', ToastAndroid.SHORT);
          setTimeout(() => { BackHandler.exitApp(); }, 1000);
          return true;
        }
    }

    async checkUserRole() {
        try {
            
            var loginCredentials = await AsyncStorage.getItem(LOGIN_CREDENTIALS);
            if (loginCredentials) {
            var login_credentials = JSON.parse(loginCredentials);
            this.setState({ userRole: login_credentials.role, fam_id: login_credentials.family_id }); 
            this.getBaseUrl();
            } else {
               
            }
        } catch (error) {
            console.error('Something went Wrong in Get Section' + error);
        }
    }

    async getBaseUrl() {
        try {
            let baseUrl = await AsyncStorage.getItem(DATA_BASE_URL);
            this.setState({ baseUrl: baseUrl });
            this.getMemberList(baseUrl);
            
        } catch (error) {
            console.error('Something went Wrong in Get Section' + error);
        }
    }
    
    async getMemberList(baseUrl) {
        
        try {
            const memberList = await AsyncStorage.getItem(DATA_LIST_MEMBERS);
            if (memberList === null) {
                 this.callApiMembers(baseUrl);
            } else {
            this.setState({ loadingStatus: false });
            }
        } catch (error) {
            console.error(error);
        }
    }
    
    callApiMembers(baseUrl) {
    
        // this.setState({ loadingStatus: true });
        var endpoint = 'list_of_members.php';
        var url = baseUrl + endpoint;
        var data = new FormData();
        data.append('hashcode', '##church00@');
        data.append('Fam_Id', 1);
        fetch(url, {
        method: 'POST',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'multipart/form-data',
        },
        body: data,
        })
        .then((response) => response.json())
        .then((responseJson) => {
            console.log('seeet2 : ' + responseJson);
            console.log(responseJson);
            if (responseJson.status === 200) {
            this.storeItems(responseJson.response, DATA_LIST_MEMBERS);
            }
            else {
            const error = 'Async Storage Failed';
            }
        })
        .catch((error) => {
            console.error(error);
        });
    }
    
    //this function for save the array response of members list to AsyncStorage
    async storeItems(response, KEY) {
        await AsyncStorage.setItem(KEY, JSON.stringify(response));
        // this.setState({ loadingStatus: false });
    }

    renderPlusButton() {
        if (this.state.userRole === 'priest') {

            var width = MAX_WIDTH - (MAX_WIDTH/5);
            var height = MAX_HEIGHT - (MAX_HEIGHT/5);
            //(70 + 90);
            return (
                <TouchableOpacity  onPress={()=> Actions.VisitNotification()} style={{ position: 'absolute', marginLeft: width, marginTop: height }}>
                
                    <View style={{ width: 60, height: 60, justifyContent: 'center', alignItems: 'center', backgroundColor: '#0ea0cc', borderRadius: 50 }}>
                        <Text style={{ fontSize: 25, color: '#fff', fontWeight: 'bold'}}>+</Text>
                    </View>
                
                </TouchableOpacity>
            )
        }
    }  

    render() {
        if (this.state.loadingStatus) {
            return (
            <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                <ActivityIndicator size='large' />
            </View>
            )  
        } else {
            return (
                <View style={{ flex: 1 }}>
                <ScrollView style={{ flex: 1 }} showsVerticalScrollIndicator={false}>
                <View style={{ backgroundColor: '#7BCFE8', borderBottomLeftRadius: 20, borderBottomRightRadius: 20 }}>
                <View style={{ flexDirection: 'row', alignItems: 'center', paddingLeft: 10, backgroundColor: '#7BCFE8', height: 80, flexDirection: 'row' }}>
                <Text style={{ fontSize: 25, fontWeight: 'bold' ,marginLeft:10}}>Hamanahel</Text>

                </View>
                <View style={{ flexDirection: 'row', alignItems: 'center'}}>
                    <Text style={{ paddingTop: 1, paddingLeft: 22,}}> Notifications</Text>
                    
                </View>
                        <View style={{ justifyContent: 'center', alignItems: 'center', backgroundColor:'#7BCFE8',height: 280, borderBottomLeftRadius: 20, borderBottomRightRadius: 20, borderTopLeftRadius: 0, borderTopRightRadius: 0}}>
                            <Notifications role={this.state.userRole}/> 
                        </View>
                    </View>
                    <ListMembers/>
                </ScrollView>
                {this.renderPlusButton()}
                </View>
            );
        }
    }
}