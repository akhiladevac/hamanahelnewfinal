// import React, { Component } from 'react';
// import { View, Text , Image, ScrollView, ActivityIndicator, TouchableOpacity,BackHandler, TextInput, Alert, Picker } from 'react-native';
// import Icon from 'react-native-vector-icons/FontAwesome5';
// import { Actions } from 'react-native-router-flux';
// import DatePicker from 'react-native-datepicker'
// import AsyncStorage from '@react-native-community/async-storage';


// const DATA_BASE_URL = '@base_url';
// export default class EditVisit extends Component{


//   constructor() {
//     super();

//     this.state = {
//         Fam_Id: '',
//         description: '',
//         date: '',
//         hour: '',
//         mint: '',
//         mode: 'am',
//         title: '',
//         visitid: '',
//         time: '',
      

//     }
// }

// componentDidMount() {
//   this.getBaseUrl();
//   const { Fam_Id, Visit_Id, title, description, time, visit_date} = this.props.datas;
//   console.log(this.props.datas);
//   this.backHandler = BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);     

//   this.setState({ Fam_Id: Fam_Id, Visit_Id: Visit_Id , title: title, description: description, date: visit_date, time: time })
// }


// componentWillUnmount() {
//   this.backHandler.remove();
// }

// handleBackPress = () => {
 
//   if (Actions.currentScene !== 'EditVisit') {
//       Actions.pop();
//   } else {
//     Actions.VisitNotification();

//     return true;
//   }
// }
// async getBaseUrl() {
//   try {
//       let baseUrl = await AsyncStorage.getItem(DATA_BASE_URL);
//       this.setState({ baseUrl: baseUrl });
      
//   } catch (error) {
//       console.error('Something went Wrong in Get Section' + error);
//   }
// }

  


//   updateButton(){
//     // const time = this.state.hour +':'+ this.state.mint +' '+ this.state.mode
//     const {Fam_Id,title, description, date, Visit_Id, time}=this.state
//     if( description && date && time && Fam_Id && title && Visit_Id){
//       this.apiCall(this.state.baseUrl)
//     }
//     else{
//       Alert.alert(' fields are empty');
//     }
//   }


//     apiCall(baseUrl) {
//    // var time = this.state.hour +':'+ this.state.mint +' '+ this.state.mode
//     const { Fam_Id, title, description, date, Visit_Id, time}=this.state
//     var endpoint = 'edit_visit.php';
//     var url = baseUrl + endpoint;
//     // console.log(url);
//     var data = new FormData();
//     data.append('hashcode', '##church00@');
//     data.append('Fam_Id', Fam_Id);
//     data.append('visit_id', Visit_Id);
//     data.append('title', title);
//     data.append('description', description);
//     data.append('date', date);
//     data.append('time', time);
//     // console.log(data);
//     console.log(data);
//     fetch(url, {
//     method: 'POST',
//     headers: {
//         Accept: 'application/json',
//         'Content-Type': 'multipart/form-data',
//     },
//     body: data,
//     })
//     .then((response) => response.json())
//     .then((responseJson) => {
//         this.setState({ response: responseJson.response, loadingStatus: false});
//         console.log(responseJson);
//         if (responseJson.status === 200){
//           Alert.alert('Visit updated')
//           Actions.pop();
//         }
//         else
//         {
//           Alert.alert('faild')
//         }
//     })
//     .catch((error) => {
//       console.error(error);
//     });
//   }



//         render() {
//             const { datas } = this.props;
//             console.log(this.props);
//             var abc = 0;
//               if (datas.Photo) {
//                   var profPic =  { uri: this.props.baseUrl + datas.Photo };
//               } else {
//                   var profPic = require('../../img/avatar.png');
//               }
//             return(
//               <ScrollView style={{ flex: 1}}>
//                   <View style={{ backgroundColor: '#7BCFE8', height: 300, alignItems: 'center', justifyContent: 'center', borderBottomEndRadius: 15, borderBottomStartRadius: 15}}>
                     
//                       <View style={{width: '80%', height: '80%', justifyContent: 'center', alignItems: 'center'}}>
//                       <Image
//                         style={{ marginTop: 20, height: 101, width: 101, borderRadius: 50 }}
//                         source={profPic}
//                         //source={{ uri: this.state.image }}
//                       />
//                         <Text style={{fontSize: 25, fontWeight: 'bold', marginTop: 20}}>
//                           {datas.Fam_Owner}
                          
//                         </Text>
//                         <Text style={{fontSize: 15, }}>
//                           {datas.Fam_Name}
//                         </Text>
//                       </View>
//                       </View>
//                       <View style={{marginTop: 8, borderTopEndRadius: 15, borderTopStartRadius: 15, height: '100%', backgroundColor: '#fff'}}>
//                       <View style={{flexDirection: 'column', justifyContent: 'center', height: 50, marginTop: 15 }}>                         
//                         <Text style={{ marginLeft: 20}}>
//                             Title
//                           </Text>
//                           <TextInput style={{ borderRadius: 2, height: 40, paddingLeft: 10, marginLeft: 20, marginRight: 20, borderWidth: 1,  marginTop: 2}}
//                           onChangeText={title => this.setState({title:title})}
//                           value={this.state.title}
//                           /> 
//                       </View>
//                       <View style={{flexDirection: 'column', justifyContent: 'center', marginTop: 10}}> 
//                         <Text style={{ marginLeft: 20}}>
//                             Discription
//                           </Text>
//                           <TextInput style={{ borderRadius: 2, height: 170, paddingLeft: 10, marginLeft: 20, marginRight: 20, borderWidth: 1, marginTop: 2, textAlignVertical: 'top'}}
//                           onChangeText={description => this.setState({ description: description})}
//                           value={this.state.description}
//                           numberOfLines={1} 
//                           multiline={true}
//                           />                       
//                       </View>
//                       <View style={{ flexDirection: 'row', justifyContent: 'center'}}>
//                       <View style={{flexDirection: 'column', justifyContent: 'center', height: 50, marginTop: 10}}> 
//                           <Text>
//                               Date
//                           </Text>
//                           <DatePicker
//                               style={{ borderWidth: .5, marginTop: 2,}}
//                               date={this.state.date}
//                               mode="date"
//                               placeholder="select date"
//                               format="YYYY-MM-DD"
//                               minDate="2000-01-01"
//                               maxDate="2030-12-31"
//                               confirmBtnText="Confirm"
//                               cancelBtnText="Cancel"
//                               customStyles={{
//                                 dateIcon: {
//                                   position: 'absolute',
//                                   left: 0,
//                                   top: 4,
//                                   // marginLeft: 5
//                                 },
//                                 dateInput: {
//                                   marginLeft: 20
//                                 }
//                                 // ... You can check the source to find the other keys.
//                               }}
//                               onDateChange={(date) => {this.setState({ date: date})}}
//                           />   

                                           
//                       </View>
//                       <View style={{flexDirection: 'column', justifyContent: 'center', height: 50, marginTop: 10}}> 
//                           <Text style={{ marginLeft: 20, }}>
//                               Time
//                           </Text>
//                           <DatePicker
//                               style={{ borderWidth: .5, marginLeft: 20, marginTop: 2}}
//                               date={this.state.time}
//                               mode="time"
//                               placeholder="select time"
//                               confirmBtnText="Confirm"
//                               cancelBtnText="Cancel"
//                               customStyles={{
//                                 dateIcon: {
//                                   position: 'absolute',
//                                   left: 0,
//                                   top: 4,
//                                   // marginLeft: 5
//                                 },
//                                 dateInput: {
//                                   marginLeft: 0
//                                 }
//                                 // ... You can check the source to find the other keys.
//                               }}
//                               onDateChange={(time) => {this.setState({ time: time})}}
//                           />   

                                           
//                       </View>
//                       </View>
//                       {/* <View style={{flexDirection: 'row', justifyContent: 'center', alignItems: 'center', height: 50, marginTop: 20 }}> 
//                           <Text >
//                               Time
//                           </Text>                    
//                       </View> */}
                
//                 <View style={{ height: 40,  marginLeft: 10, marginRight: 10, borderRadius: 12, marginBottom: 80, marginTop: 15 }}>
//                    <View style={{ flexDirection: 'row', justifyContent: 'flex-end' }}>
//                       <TouchableOpacity onPress={()=> this.updateButton()} style={{ backgroundColor: '#7BCFE8', margin:5, alignItems: 'center',height:40, width: 80, marginRight: 10, borderRadius: 15, justifyContent: 'center',alignItems: 'center', borderWidth: 1, borderColor: '#fff'}}>
//                         <Text style={{fontWeight: 'bold'}} >
//                           Update
//                         </Text>
                        
//                       </TouchableOpacity>
//                    </View>
//                    </View>
//                    </View>
//                    </ScrollView>
                   

//             );

                    
//         }
//     }
import React, { Component } from 'react';
import { View, Text , Image, ScrollView, ActivityIndicator,BackHandler,NetInfo, TouchableOpacity, TextInput, Alert, Picker } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome5';
import { Actions } from 'react-native-router-flux';
import DatePicker from 'react-native-datepicker'
import AsyncStorage from '@react-native-community/async-storage';


const DATA_BASE_URL = '@base_url';
export default class EditVisit extends Component{


  constructor() {
    super();

    this.state = {
      description: '',
      date: '',
      title: '',
      id: '',
      connection_Status : '',
          settimeout:false,
          loadingStatus: false,
          PickerValueHolder:'',
          response3:[],
          category:''

      

    }
}
UpdateData = () => {
  this.props.parentCallback("juhiu");

}
componentDidMount() {
  
  this.checkNetwork();

  this.getBaseUrl();
  this.setState({ role: this.props.datas.role, id: this.props.datas.id, category: this.props.datas.category,  title: this.props.datas.title, description: this.props.datas.description, date: this.props.datas.expire, time: this.props.datas.time })
   this.backHandler = BackHandler.addEventListener('hardwareBackPress', this.handleBackPress); 
}
checkNetwork=() => {
  NetInfo.isConnected.addEventListener(
    'connectionChange',
    this._handleConnectivityChange

);

NetInfo.isConnected.fetch().done((isConnected) => {

  if(isConnected == true)
  {
    this.setState({connection_Status : "Online"})
  }
  else
  {
    this.setState({connection_Status : "Offline"})



  }

});

}

componentWillUnmount() {
  this.backHandler.remove();
  NetInfo.isConnected.removeEventListener(
      'connectionChange',
      this._handleConnectivityChange

  );

}

_handleConnectivityChange = (isConnected) => {

  if(isConnected == true)
    {
      this.setState({connection_Status : "Online"})

    }
    else
    {
      this.setState({connection_Status : "Offline"})

      


    }
};
 
choosecategory(baseUrl){
  var endpoint = 'member_categories.php';
  var url = baseUrl + endpoint;
  // console.log(url);
  var data = new FormData();
  data.append('hashcode', '##church00@');
  fetch(url, {
  method: 'POST',
  headers: {
      Accept: 'application/json',
      'Content-Type': 'multipart/form-data',
  },
  body: data,
  })
 

  .then((response) => response.json())
  
  .then((responseJson) => {
      // console.log(responseJson);
      this.setState({ 
          response3: responseJson.response, 
          loadingStatus: false,
      })
       console.log(responseJson);
  }) 
  
  .catch((error) => {
    console.error(error);
  });
  // setTimeout(() => ab
  
}
async getBaseUrl() {
  try {
      let baseUrl = await AsyncStorage.getItem(DATA_BASE_URL);
      this.setState({ baseUrl: baseUrl });
      this.choosecategory(this.state.baseUrl)
      
  } catch (error) {
      console.error('Something went Wrong in Get Section' + error);
  }
}


handleBackPress = () => {
 
  if (Actions.currentScene !== 'EditNotification') {
    Actions.pop();
} else {
  Actions.VisitNotification();

  return true;
}
}
  


updateButton(){
  const {title, description, date, id,category}=this.state
  if( description && date &&  title && id && category){
    
    this.apiCall(this.state.baseUrl)
  }
  else{
    Alert.alert(' fields are empty');
  }
}


    apiCall(baseUrl) {
              this.setState({ loadingStatus: true })

   // var time = this.state.hour +':'+ this.state.mint +' '+ this.state.mode
   const {  title, description, date,role,  id,category}=this.state
   var endpoint = 'edit_notification.php';
   var url = baseUrl + endpoint;
   // console.log(url);
   var data = new FormData();
   data.append('hashcode', '##church00@');
   data.append('notificationid', id);
   data.append('title', title);
   data.append('description', description);
   data.append('expire', date);
    data.append('category', category);
 
   // console.log(data);
   // console.log(data);
   fetch(url, {
   method: 'POST',
   headers: {
       Accept: 'application/json',
       'Content-Type': 'multipart/form-data',
   },
   body: data,
   })
   .then((response) => response.json())
   .then((responseJson) => {
       this.setState({ response: responseJson.response, loadingStatus: false});
        console.log(responseJson);
        if (responseJson.status === 200){
          Alert.alert('success')
          this.UpdateData();
          Actions.pop();
        }
        else
        {
         Alert.alert('faild')
        }
   })
   .catch((error) => {
     console.error(error);
   });
   setTimeout(() => {
     this.setState({
         loadingStatus:false,
         settimeout:true
     })
     },45000)
 
   }
  renderUpdateButton() {
    if (this.state.loadingStatus) {
        return (
            
            
                <View style={{ width: '100%'}}> 
                    <View style={{ backgroundColor: '#7BCFE8', width: '100%', height: 60, justifyContent: 'center', alignItems: 'center' }}>
                        <ActivityIndicator size='small' />
                    </View>
                </View>
           
        )
    } else if(this.state.loadingStatus==false && this.state.settimeout == true && this.state.connection_Status=='Offline'  ){

return (
    <TouchableOpacity onPress={ () => this.updateButton()}>
        <View style={{ width: '100%'}}> 
            <View style={{ backgroundColor: '#7BCFE8', width: '100%', height: 60, justifyContent: 'center', alignItems: 'center' }}>
                <Text style={{ fontSize: 15, fontWeight: '500' }}> You are offline </Text>
                <Icon style={{ marginRight:0}} name="undo" size={17} color="#fff" />

            </View>
        </View>
    </TouchableOpacity>
)  
}else {
    
        return (
          <View style={{ width: '100%'}}> 
          <TouchableOpacity onPress={ () => this.updateButton()} style={{backgroundColor: '#7BCFE8',height: 60, alignItems: 'center', justifyContent: 'center',   borderRightWidth: 1}}>
              <Text style={{ fontSize: 17, fontWeight: 'bold'}}>
                  UPDATE
              </Text>
          </TouchableOpacity>
          </View>
          
        )
    }
}

        render() {
            const { datas } = this.props;
            console.log(this.props);
           
            return(
              <View style={{ flex: 1}} >
                <ScrollView showsVerticalScrollIndicator={false}>

              
               <View style={{ alignItems: 'center', width: '100%', height: '61%',marginTop:120}}>
               <View style={{ backgroundColor: '#fff', height: 70, borderRadius: 10, marginTop: 10, width: '95%', }}>
                       <Text style={{ paddingTop: 10, paddingLeft: 20, fontWeight: 'bold', color: '#000' }}> Category</Text>
                       <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                           {/* <TextInput
                               style={{ paddingTop: 10, paddingLeft: 20, width: '80%', paddingRight: 20 }} 
                               onChangeText={title => this.setState({title:title})}
                               value={this.state.title}
                           /> */}
                           {/* <Text style={{width:80,height:50,marginLeft:25,marginTop:6}}>{this.state.category}</Text> */}
                           <Picker style={{width:200,height:50,marginLeft:15,marginBottom:20}}
            selectedValue={this.state.category}
 
            onValueChange={(itemValue, itemIndex) => this.setState({category: itemValue})} >
 
            { this.state.response3.map((item, key)=>(
            <Picker.Item label={item.position} value={item.position} key={key} />)
            )}
    
          </Picker>
                       </View>
                   </View>
                   <View style={{ backgroundColor: '#fff', height: 70, borderRadius: 10, marginTop: 10, width: '95%', }}>
                       <Text style={{ paddingTop: 10, paddingLeft: 20, fontWeight: 'bold', color: '#000' }}> Title</Text>
                       <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                           <TextInput
                               style={{ paddingTop: 10, paddingLeft: 20, width: '80%', paddingRight: 20 }} 
                               onChangeText={title => this.setState({title:title})}
                               value={this.state.title}
                           />
                       </View>
                   </View>
                   <View style={{ backgroundColor: '#fff', height: 200, borderRadius: 10, marginTop: 5, width: '95%', }}>
                       <Text style={{ paddingTop: 5, paddingLeft: 20, fontWeight: 'bold', color: '#000' }}> Description</Text>
                       <View style={{ flexDirection: 'row' }}>
                           <TextInput
                               style={{ textAlignVertical: 'top', paddingTop: 10, paddingLeft: 20, width: '100%', height: 170, paddingRight: 20}} 
                               onChangeText={description => this.setState({ description: description})}
                               value={this.state.description}
                               numberOfLines={1} 
                               multiline={true}
                           />
                       </View>
                   </View>
                   <View style={{ flexDirection: 'row', justifyContent: 'center', marginTop: 0}}>
                      <View style={{flexDirection: 'column', justifyContent: 'center', height: 50, marginTop: 10}}> 
                          <Text>
                              Date
                          </Text>
                          <DatePicker
                               style={{  marginTop: 2, backgroundColor: '#fff'}}
                               date={this.state.date}
                               mode="date"
                               placeholder="select date"
                               format="YYYY-MM-DD"
                               minDate="2000-01-01"
                               maxDate="2030-12-31"
                               confirmBtnText="Confirm"
                               cancelBtnText="Cancel"
                               customStyles={{
                                dateIcon: {
                                  position: 'absolute',
                                  left: 0,
                                  top: 4,
                                  // marginLeft: 5
                                },
                                dateInput: {
                                  
                                }
                                // ... You can check the source to find the other keys.
                              }}
                              onDateChange={(date) => {this.setState({ date: date})}}
                          />   

                                           
                      </View>
                    </View>
               </View>
               
               </ScrollView>
                   {this.renderUpdateButton()}

             </View>
                   

            );

                    
        }
    }