
import React, { Component } from 'react';
import { View, Text, FlatList, TouchableOpacity, Image, TextInput,BackHandler } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import AsyncStorage from '@react-native-community/async-storage';
import { Actions } from 'react-native-router-flux';

const DATA_LIST_MEMBERS = '@members_list';
const DATA_BASE_URL = '@base_url';

export default class FatherUserList1 extends Component {

    constructor() {
        super();

        this.state = {
            loading: false,      
            data: [],
            temp: [],      
            error: null,  
            baseUrl: '',  
            pageMode2:''
          };

        this.arrayholder = [];
    }

    componentDidMount() {
      this.setState({
        pageMode2:this.props.pageMode1
      })
      var page=this.props.pageMode1;
     this.getBaseUrl();
        this.getMembersList();
        this.backHandler = BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);     

    }
    componentWillUnmount() {
      this.backHandler.remove();
    }
    
    handleBackPress = () => {
     
      if (Actions.currentScene !== 'FatherUserList1') {
          Actions.pop();
      } else {
        Actions.VisitNotification({data:this.state.pageMode2});
    
        return true;
      }
    }
    async getBaseUrl() {
      try {
          let baseUrl = await AsyncStorage.getItem(DATA_BASE_URL);
          this.setState({ baseUrl: baseUrl });
      } catch (error) {
          console.error('Something went Wrong in Get Section' + error);
      }
    }

    async getMembersList() {
        this.setState({ loading: true });
        try {
            const membersList = await AsyncStorage.getItem(DATA_LIST_MEMBERS);
            if (membersList !== null) {
              const abc = JSON.parse(membersList);
            this.setState({          
                data: abc,  
                temp: abc,        
                error: null,          
                loading: false,
                query: '',        
              });  
              this.arrayholder = abc; 
              //console.log(this.state.data)
            } 
        } catch (error) {
            console.log('Something went Wrong Saving list');
        }
    }

    searchFunction(query) {
        
        if (query.length === 0) {
            this.setState({ data: this.state.temp, query: query });
        } else {
            this.setState({ query: query, });
            this.searchFilterFunction(this.state.query);
        }
        
    }

    searchFilterFunction = text => {    
        const newData = this.arrayholder.filter(item => {      
          const itemData = `${item.Fam_Owner.toUpperCase()}   
                            ${item.Fam_Name.toUpperCase()}`; 
          
           const textData = text.toUpperCase();
            
           return itemData.indexOf(textData) > -1;    
        });
        
        this.setState({ data: newData });  
      };

      renderProperties = ({ item }) => {
        if (item.members[0].photo) {
          var profPic =  { uri: this.state.baseUrl + item.members[0].photo };
      }  else {
          var profPic = require('../../img/avatar.png');
      }
        return (
          <TouchableOpacity onPress={() => Actions.AddVisit({ datas: item, baseUrl: this.state.baseUrl,callbackstate1:this.props.callbackstate ,pageMode2: this.props.pageMode1})}>
            <View style={{ backgroundColor: '#fff', height: 70, marginTop: 10, marginLeft: 10, marginRight: 10, borderRadius: 10 }}>
              <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center' }}>
                  <Image 
                  style={{ width: 50, height: 50, marginLeft: 10, borderRadius: 20 }}
                  source={profPic}
                  />
                  <View>
                      <Text style={{ paddingLeft: 15, fontSize: 18, fontWeight: '500' }}>{ item.Fam_Owner }</Text>
                      <Text style={{ paddingLeft: 15, paddingTop: 8 }}>{ item.Fam_Name } House</Text>
                  </View>
                
              </View>
            </View>
          </TouchableOpacity>
        );
      };

    render() {
        return (
            <View style={{ borderTopWidth: 0, borderBottomWidth: 0 }}>
            
                <View style={{ backgroundColor: '#7BCFE8', }}>
                <View style={{ marginTop: 5, alignItems: 'center'}}>
                  <Text style={{fontSize: 20, fontWeight: 'bold'}}>
                    Choose Family
                  </Text>
                </View>
                <View style={{ flexDirection: 'row', alignItems: 'center', paddingLeft: 10, backgroundColor: '#fff', height: 40, margin: 20, marginTop: 15, borderRadius: 50, flexDirection: 'row' }}>
                    <TextInput
                      style={{ width: '90%', fontSize: 16, color: '#000' }} 
                      placeholder='Search'
                      onChangeText={text => this.searchFunction(text)}
                      autoCorrect={false}
                      // onChangeText={(query) => this.setState({ search: query })}
                    />
                    <Icon style={{ color: '#000'}} name={'search'} size={20} />
                </View>
                </View>
                <FlatList   
                    style={{ marginTop: 2 }}       
                    data={this.state.data}          
                    renderItem={this.renderProperties}          
                    keyExtractor={(item, index) => index.toString()}  
                    ItemSeparatorComponent={this.renderSeparator}                             
                />            
            </View>
        );
    }
}