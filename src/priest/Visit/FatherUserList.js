
import React, { Component } from 'react';
import { View, Text, FlatList, TouchableOpacity, Image, TextInput } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import AsyncStorage from '@react-native-community/async-storage';
import { Actions } from 'react-native-router-flux';

const DATA_LIST_MEMBERS = '@members_list';

export default class FatherUserList extends Component {

    constructor() {
        super();

        this.state = {
            loading: false,      
            data: [],
            temp: [],      
            error: null,    
          };

        this.arrayholder = [];
    }

    componentDidMount() {
        this.getMembersList();
    }

    async getMembersList() {
        this.setState({ loading: true });
        try {
            const membersList = await AsyncStorage.getItem(DATA_LIST_MEMBERS);
            if (membersList !== null) {
              const abc = JSON.parse(membersList);
            this.setState({          
                data: abc,  
                temp: abc,        
                error: null,          
                loading: false,
                query: '',        
              });  
              this.arrayholder = abc; 
              // console.log(this.state.data)
            } 
        } catch (error) {
            console.log('Something went Wrong Saving list');
        }
    }

    searchFunction(query) {
        
        if (query.length === 0) {
            this.setState({ data: this.state.temp, query: query });
        } else {
            this.setState({ query: query, });
            this.searchFilterFunction(this.state.query);
        }
        
    }

    searchFilterFunction = text => {    
        const newData = this.arrayholder.filter(item => {      
          const itemData = `${item.Fam_Owner.toUpperCase()}   
                            ${item.Fam_Name.toUpperCase()}`; 
          
           const textData = text.toUpperCase();
            
           return itemData.indexOf(textData) > -1;    
        });
        
        this.setState({ data: newData });  
      };

      renderProperties = ({ item }) => {
        return (
          <TouchableOpacity onPress={() => Actions.AddNotification({ namez:item.Fam_Name, id:item.Fam_Id})}>
            <View style={{ backgroundColor: '#fff', height: 70, marginTop: 10, marginLeft: 10, marginRight: 10, borderRadius: 10 }}>
              <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center' }}>
                  <Image 
                  style={{ width: 50, height: 50, marginLeft: 10 }}
                  source={require('../../img/avatar.png')}
                  />
                  <View>
                      <Text style={{ paddingLeft: 15, fontSize: 18, fontWeight: '500' }}>{ item.Fam_Owner }</Text>
                      <Text style={{ paddingLeft: 15, paddingTop: 8 }}>{ item.Fam_Name } House</Text>
                  </View>
                
              </View>
            </View>
          </TouchableOpacity>
        );
      };

    render() {
        return (
            <View style={{ borderTopWidth: 0, borderBottomWidth: 0 }}>
            
                <View style={{ backgroundColor: '#7BCFE8'}}>
                <View style={{ flexDirection: 'row', alignItems: 'center', paddingLeft: 10, backgroundColor: '#fff', height: 40, margin: 20, marginTop: 50, borderRadius: 50, flexDirection: 'row' }}>
                    <TextInput
                      style={{ width: '90%', fontSize: 16, color: '#000' }} 
                      placeholder='Search'
                      onChangeText={text => this.searchFunction(text)}
                      autoCorrect={false}
                      // onChangeText={(query) => this.setState({ search: query })}
                    />
                    <Icon style={{ color: '#000'}} name={'search'} size={20} />
                </View>
                </View>
                <FlatList   
                    style={{ marginTop: 2 }}       
                    data={this.state.data}          
                    renderItem={this.renderProperties}          
                    keyExtractor={(item, index) => index.toString()}  
                    ItemSeparatorComponent={this.renderSeparator}                             
                />            
            </View>
        );
    }
}