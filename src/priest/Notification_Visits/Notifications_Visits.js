
import React, { Component } from 'react';
import { View, Text, FlatList, TouchableOpacity, Image, Dimensions,NetInfo, BackHandler,TextInput,Alert,ActivityIndicator } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import AsyncStorage from '@react-native-community/async-storage';
import { Actions } from 'react-native-router-flux';

const DATA_LIST_MEMBERS = '@members_list';
const DATA_BASE_URL = '@base_url';

const MAX_HEIGHT = Dimensions.get('window').height;
const MAX_WIDTH = Dimensions.get('window').width;
const LOGIN_CREDENTIALS = '@login_credentials';

export default class Notification_Visits extends Component {

    constructor() {
        super();
        this.viewNotificationApiCallUpdate = this.viewNotificationApiCallUpdate.bind(this)
        this.viewVisitsApiCallUpdate = this.viewVisitsApiCallUpdate.bind(this)
        this.tabClicks = this.tabClicks.bind(this)


        this.state = {
          fam_id:'',
          visit_id:'',
            loadingStatus: true, 
            loadingStatus2: true,
            baseUrl: '',     
            data: [],
            temp: [],      
            error: null,  
            switch: false,
            pageMode: true,
            query: '', 
            notificationData: [],   
            connection_Status : '',
            settimeout:false,
      
             //true for events and false for wishlists  
          };

        this.arrayholder = [];
    }
    componentDidMount() {
      if(this.props.data ==null){
      this.setState({
        pageMode:true
      })
    }else{
      this.setState({
         pageMode:false
     })
    }
      this.checkNetwork();
    }
    deletealertNotification(response,r){
      Alert.alert(  
        'Delete ',  
        'Do you really want to delete this notification?',  
        [  
            {  
                text: 'No',  
                onPress: () => console.log('Cancel Pressed'),  
                style: 'cancel',  
            },  
            {text: 'Yes', onPress: () => this.deletenotification(response,r)},  
        ]  
    );  
      }
      deletealertvisit(response,r,f){
        Alert.alert(  
          'Delete ',  
          'Do you really want to delete this visit?',  
          [  
              {  
                  text: 'No',  
                  onPress: () => console.log('Cancel Pressed'),  
                  style: 'cancel',  
              },  
              {text: 'Yes', onPress: () => this.deletevisit(response,r,f)},  
          ]  
      );  
        }
        deletevisit(response,r,f){
          this.setState({
              visit_id:r,
              fam_id:f
          })
          var endpoint = 'delete_visits.php';
          var url =this.state.baseUrl + endpoint;
          // console.log(url);
          var data = new FormData();
          data.append('hashcode', '##church00@');
          data.append('Fam_Id',this.state.fam_id);

           data.append('visit_id',this.state.visit_id);
    
          fetch(url, {
          method: 'POST',
          headers: {
              Accept: 'application/json',
              'Content-Type': 'multipart/form-data',
          },
          body: data,
          })
         
    
          .then((response) => response.json())
          
          .then((responseJson) => {
              // console.log(responseJson);
              this.setState({ 
                  deletenotifresponse: this.state.response, 
                  loadingStatus: false,
              })
              Alert.alert("Deleted Successfully")
              this.viewVisitsApiCall(this.state.baseUrl);
    
              // console.log(responseJson);
          }) 
          
          .catch((error) => {
            console.error(error);
          });
          // setTimeout(() => a
    
      }
      deletenotification(response,r){
      this.setState({
          notification_id:r
      })
      var endpoint = 'delete_notification.php';
      var url =this.state.baseUrl + endpoint;
      // console.log(url);
      var data = new FormData();
      data.append('hashcode', '##church00@');
       data.append('notificationid',this.state.notification_id  );

      fetch(url, {
      method: 'POST',
      headers: {
          Accept: 'application/json',
          'Content-Type': 'multipart/form-data',
      },
      body: data,
      })
     

      .then((response) => response.json())
      
      .then((responseJson) => {
          // console.log(responseJson);
          this.setState({ 
              deletenotifresponse: this.state.response, 
              loadingStatus: false,
          })
          Alert.alert("Deleted Successfully")
          this.viewNotificationApiCall(this.state.baseUrl);

          // console.log(responseJson);
      }) 
      
      .catch((error) => {
        console.error(error);
      });
      // setTimeout(() => a

  }
    viewNotificationApiCallUpdate(){
      this.getBaseUrl();
    }
    componentWillMount() {
        this.getBaseUrl();
        console.log(Actions.currentScene)
        this.backHandler = BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);     

        //this.getMembersList();
    }
    checkNetwork=() => {
      NetInfo.isConnected.addEventListener(
        'connectionChange',
        this._handleConnectivityChange
  
    );
   
    NetInfo.isConnected.fetch().done((isConnected) => {
  
      if(isConnected == true)
      {
        this.setState({connection_Status : "Online"})
      }
      else
      {
        this.setState({connection_Status : "Offline"})
  
  
  
      }
  
    });
  
    }
    componentWillUnmount() {
  
      NetInfo.isConnected.removeEventListener(
          'connectionChange',
          this._handleConnectivityChange
   
      );
   
    }
   
    _handleConnectivityChange = (isConnected) => {
   
      if(isConnected == true)
        {
          this.setState({connection_Status : "Online"})
  
        }
        else
        {
          this.setState({connection_Status : "Offline"})
  
          
  
  
        }
    };
   
  
  

    
    handleBackPress = () => {
     
      if (Actions.currentScene !== 'VisitNotification') {
          Actions.pop();
      } else {
        
        Actions.Home()
        return true;
      }
     }
     viewVisitsApiCallUpdate(){
       this.getBaseUrl();
     }
    async getBaseUrl() {
        try {
            let baseUrl = await AsyncStorage.getItem(DATA_BASE_URL);
            this.setState({ baseUrl: baseUrl });

           
    
            this.viewNotificationApiCall(baseUrl);
            this.viewVisitsApiCall(baseUrl);
        } catch (error) {
            console.error('Something went Wrong in Get Section' + error);
        }
      }

        viewNotificationApiCall(baseUrl) {
        var endpoint = 'notification.php';
        var url = baseUrl + endpoint;
        // console.log(url);
        var data = new FormData();
        data.append('hashcode', '##church00@');
        fetch(url, {
        method: 'POST',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'multipart/form-data',
        },
        body: data,
        })
        .then((response) => response.json())
        .then((responseJson) => {
            if (responseJson.status === 200) {
                this.setState({ 
                    notificationData: responseJson.response, 
                    loadingStatus: false
                });
            }
            
            // console.log(responseJson);
        })
        .catch((error) => {
          console.error(error);
        });
        setTimeout(() => {
          this.setState({
              loadingStatus:false,
              settimeout:true
          })
          },45000)
    }

      viewVisitsApiCall(baseUrl) {
        var endpoint = 'view_visits.php';
        var url = baseUrl + endpoint;
        // console.log(url);
        var data = new FormData();
        data.append('hashcode', '##church00@');
        fetch(url, {
        method: 'POST',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'multipart/form-data',
        },
        body: data,
        })
        .then((response) => response.json())
        .then((responseJson) => {
            console.log('view visits api call');
            console.log(responseJson);
            if (responseJson.status === 200) {
                // this.setState({ 
                //     notificationData: responseJson.response, 
                //     loadingStatus: false
                // });

                this.setState({          
                    data: responseJson.response,  
                    temp: responseJson.response,        
                    error: null,          
                    loadingStatus2: false,
                    query: '',        
                  });  
                  this.arrayholder = responseJson.response; 
                
            } else {
                this.setState({ 
                    loadingStatus2: false
                });
                alert.alert(responseJson.msg);
            }
            
            // console.log(responseJson);
        })
        .catch((error) => {
          console.error(error);
        });
        setTimeout(() => {
          this.setState({
              loadingStatus:false,
              settimeout:true
          })
          },45000)
    }

    async getMembersList() {
        this.setState({ loading: true });
        try {
            const membersList = await AsyncStorage.getItem(DATA_LIST_MEMBERS);
            if (membersList !== null) {
              const abc = JSON.parse(membersList);
            this.setState({          
                data: abc,  
                temp: abc,        
                error: null,          
                loading: false,
                query: '',        
              });  
              this.arrayholder = abc; 
              // console.log(this.state.data)
            } 
        } catch (error) {
            console.log('Something went Wrong Saving list');
        }
    }

    renderPropertiesNotifications = ({item}) => {
        if (item.date !== '') {
            const dataData = item.date.split('-');
            var date = dataData[2] + '-' + dataData[1] + '-' + dataData[0];
        } else {
            var data = '';
        }
        // if (item.photo) {
        //   var profPic =  { uri: this.state.baseUrl + item.photo };
        // } else {
              var profPic = require('../../img/man.png');
          // }
        return (
            <TouchableOpacity  onLongPress={() =>this.deletealertNotification(this.state.baseUrl,item.id)}

            onPress={() => Actions.EditNotification({ datas: item,parentCallback: this.viewNotificationApiCallUpdate })} style={{ backgroundColor: '#fff', height: 70, marginTop: 10, marginLeft: 10, marginRight: 10, borderRadius: 10 }}>
                <View style={{ flexDirection: 'row', borderRadius: 10, alignItems: 'center' }}>

                    <Image 
                        style={{ width: 60, height: 60, borderRadius: 30, marginLeft: 10, marginTop: -10 }}
                        source={profPic}
                    />

                    <View style={{ width: 200, height: 70, justifyContent: 'center' }}>
                        <Text numberOfLines={1} style={{ paddingLeft: 15, fontSize: 18, fontWeight: '500' }}>{ item.title }</Text>
                        <Text numberOfLines={1} style={{ paddingLeft: 15, paddingTop: 8, marginBottom: 20 }}>{ item.category }</Text>
                    </View>

                    <View style={{ width: 200, height: 70, justifyContent: 'center' }}>
                        <Text style={{fontSize: 11, paddingLeft: 20 }}>{item.date}</Text>
                    </View>

                </View>
                <View style={{ height: 0.2, backgroundColor: 'grey', width: '100%'}}></View>
            </TouchableOpacity>
        )
    }

    searchFunction(query) {
        
        if (query.length === 0) {
            this.setState({ data: this.state.temp, query: query });
        } else {
            this.setState({ query: query, });
            this.searchFilterFunction(this.state.query);
        }
        
    }

    searchFilterFunction = text => {    
        const newData = this.arrayholder.filter(items => {   
          
            const itemData = `${items.Fam_Name.toUpperCase()}   
                              ${items.Fam_Owner.toUpperCase()}`; 
            const textData = text.toUpperCase();
            return itemData.indexOf(textData) > -1; 
        });
        console.log(newData);
        this.setState({ data: newData });  
      };

      
      renderFlatListNotifications() {
        if (this.state.loadingStatus) {
          return (
            <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                <ActivityIndicator size='large' />
            </View>
          );
        }else if(this.state.loadingStatus==false && this.state.settimeout == true && this.state.connection_Status=='Offline' ){

          return (
              <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center'}}>
                  <Text style={{fontSize:18}}>You are offline</Text>
                  <View style={{ flexDirection: 'row', alignItems: 'center'}}>
                  <TouchableOpacity onPress={() => this.viewNotificationApiCall(this.state.baseUrl)}>
                      <Icon style={{ marginTop:10}} name="undo" size={17} color="#7BCFE8" />
                      </TouchableOpacity>
                  <Text style={{fontSize:18,marginTop:10,marginLeft:10}}>Retry</Text>
                  </View>

              </View>
          );
        } 
       else if(this.state.settimeout == true,this.state.connection_Status=='Online')  {
        return (
            <FlatList   
                style={{ marginTop: 5 }}       
                data={this.state.notificationData}          
                renderItem={this.renderPropertiesNotifications}          
                keyExtractor={(item, index) => index.toString()}  
                ItemSeparatorComponent={this.renderSeparator}  
                                            
            />   
          )
        }else{

        }
      }

      renderFlatListVisits() {
        if (this.state.loadingStatus2) {
          return (
            <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                <ActivityIndicator size='large' />
            </View>
          );
        }else if(this.state.loadingStatus==false && this.state.settimeout == true && this.state.connection_Status=='Offline' ){

          return (
              <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center'}}>
                  <Text style={{fontSize:18}}>You are offline</Text>
                  <View style={{ flexDirection: 'row', alignItems: 'center'}}>
                  <TouchableOpacity onPress={() => this.viewVisitsApiCall(this.state.baseUrl)}>
                      <Icon style={{ marginTop:10}} name="undo" size={17} color="#7BCFE8" />
                      </TouchableOpacity>
                  <Text style={{fontSize:18,marginTop:10,marginLeft:10}}>Retry</Text>
                  </View>

              </View>
          );
        }  
        else if(this.state.settimeout == true,this.state.connection_Status=='Online')  {
          return (
            <FlatList   
                style={{ marginTop: 5 }}       
                data={this.state.data}          
                renderItem={this.renderPropertiesVisits}          
                keyExtractor={(item, index) => index.toString()}  
                ItemSeparatorComponent={this.renderSeparator}  
                                        
            />
          )
        }else{
          return (
            <FlatList   
                style={{ marginTop: 5 }}       
                data={this.state.data}          
                renderItem={this.renderPropertiesVisits}          
                keyExtractor={(item, index) => index.toString()}  
                ItemSeparatorComponent={this.renderSeparator}  
                                        
            />
          )

        }
      }

    renderPropertiesVisits = ({item}) => {

      if (item.Photo) {
          var profPic =  { uri: this.state.baseUrl + item.Photo };
      }  else {
          var profPic = require('../../img/avatar.png');
      }

      if (item.visit_date !== '') {
        const dataData = item.visit_date.split('-');
        var date = dataData[2] + '-' + dataData[1] + '-' + dataData[0];
      } else {
          var data = '';
      }   
        
        return (
            <TouchableOpacity onLongPress={() =>this.deletealertvisit(this.state.baseUrl,item.Visit_Id,item.Fam_Id)}
            onPress={() => Actions.Visitlist({ datas: item, baseUrl: this.state.baseUrl ,parentCallback: this.viewVisitsApiCallUpdate,callbackstate:this.tabClicks,pageMode1:this.state.pageMode})} style={{ backgroundColor: '#fff', height: 70, marginTop: 10, marginLeft: 10, marginRight: 10, borderRadius: 10 }}>
                <View style={{ flexDirection: 'row', borderRadius: 10, alignItems: 'center' }}>

                    <Image 
                        style={{ width: 60, height: 60, borderRadius: 30, marginLeft: 10, marginTop: -10 }}
                        source={profPic}
                    />

                    <View style={{ width: 200, height: 70, justifyContent: 'center' }}>
                        <Text style={{ paddingLeft: 15, fontSize: 18, fontWeight: '500' }}>{ item.Fam_Owner }</Text>
                        <Text numberOfLines={1} style={{ paddingLeft: 15, paddingTop: 8, marginBottom: 20 }}>{ item.Fam_Name } House</Text>
                    </View>

                    <View style={{ width: 200, height: 70, justifyContent: 'center' }}>
                        <Text style={{fontSize: 11, paddingLeft: 20 }}>{date}</Text>
                    </View>

                </View>
                <View style={{ height: 0.2, backgroundColor: 'grey', width: '100%'}}></View>
            </TouchableOpacity>
        )
    }

    tabClicks(mode) {
      if(mode === 'notification') {
        this.setState({ pageMode: true });
        this.secondwNotificationApiCall(this.state.baseUrl);
      } else {
        this.setState({ pageMode: false });
        this.secondVisitsApiCall(this.state.baseUrl);
      }
    }

    secondwNotificationApiCall(baseUrl) {
      var endpoint = 'notification.php';
      var url = baseUrl + endpoint;
      // console.log(url);
      var data = new FormData();
      data.append('hashcode', '##church00@');
      fetch(url, {
      method: 'POST',
      headers: {
          Accept: 'application/json',
          'Content-Type': 'multipart/form-data',
      },
      body: data,
      })
      .then((response) => response.json())
      .then((responseJson) => {
          if (responseJson.status === 200) {
              this.setState({ 
                  notificationData: responseJson.response, 
              });
          }
          
          // console.log(responseJson);
      })
      .catch((error) => {
        console.error(error);
      });
  }

  secondVisitsApiCall(baseUrl) {
    var endpoint = 'view_visits.php';
    var url = baseUrl + endpoint;
    // console.log(url);
    var data = new FormData();
    data.append('hashcode', '##church00@');
    fetch(url, {
    method: 'POST',
    headers: {
        Accept: 'application/json',
        'Content-Type': 'multipart/form-data',
    },
    body: data,
    })
    .then((response) => response.json())
    .then((responseJson) => {
        console.log('view visits api call');
        console.log(responseJson);
        if (responseJson.status === 200) {
            // this.setState({ 
            //     notificationData: responseJson.response, 
            //     loadingStatus: false
            // });

            this.setState({          
                data: responseJson.response,  
                temp: responseJson.response,        
                error: null,          
                loadingStatus2: false,
                query: '',        
              });  
              this.arrayholder = responseJson.response; 
            
        } else {
            this.setState({ 
                loadingStatus2: false
            });
            alert.alert(responseJson.msg);
        }
        
        // console.log(responseJson);
    })
    .catch((error) => {
      console.error(error);
    });
}

      renderItems() {

        var width = MAX_WIDTH - (MAX_WIDTH/5);
        var height = MAX_HEIGHT - (MAX_HEIGHT/5);
        
        if (this.state.pageMode) {
            return (
              <View style={{ flex: 1, marginTop: 60 }}>
              <TouchableOpacity onPress={() => Actions.Home()}>
                <Icon style={{ color: '#000', marginLeft: 20, marginBottom: 10 }} name={'chevron-left'} size={25} />
              </TouchableOpacity>
                    <View style={{ marginLeft: 10, marginRight: 10, }}>

                        <View style={{ height: 50, backgroundColor: 'rgba(255, 255, 255, 0.6)', marginTop: 10, borderRadius: 7, justifyContent: 'center', alignItems: 'center', flexDirection: 'row' }}>
                          <TouchableOpacity onPress={() => this.tabClicks('notification')} style={{ width: '49%', height: 45, backgroundColor: '#fff',  marginRight: 2, borderRadius: 7}}>
                            <View style={{ width: '100%', height: 45, borderRadius: 7, alignItems: 'center', justifyContent: 'center'}}>
                              <Text> Notifications </Text>
                            </View>
                          </TouchableOpacity>

                          
                          <TouchableOpacity onPress={() => this.tabClicks('visits')} style={{ width: '49%', height: 45, borderRadius: 7, alignItems: 'center', marginRight: 2, justifyContent: 'center'}}>
                            <Text> Visits </Text>
                          </TouchableOpacity>
                          
                                                
                        </View>
                    </View>

                    <View style={{ flex: 1, borderTopWidth: 0, borderBottomWidth: 0, marginTop: 10, backgroundColor: '#fff', borderRadius: 7, borderBottomEndRadius: 0, borderBottomStartRadius: 0 }}>
                      
                       {this.renderFlatListNotifications()}   

                    </View>

                    <TouchableOpacity  onPress={()=> Actions.AddNotification({parentCallback: this.viewNotificationApiCallUpdate})} style={{ position: 'absolute', marginLeft: width, marginTop: height }}>
                        <View style={{ width: 60, height: 60, justifyContent: 'center', alignItems: 'center', backgroundColor: '#0ea0cc', borderRadius: 50 }}>
                            <Text style={{ fontSize: 25, color: '#fff', fontWeight: 'bold'}}>+</Text>
                        </View>
                    </TouchableOpacity>

                  </View>
            )
        } else {
          return (
            <View style={{ flex: 1, marginTop: 60 }}>
              <TouchableOpacity onPress={() => Actions.Home()}>
                  <Icon style={{ color: '#000', marginLeft: 20, marginBottom: 10 }} name={'chevron-left'} size={25} />
                </TouchableOpacity>
                    <View style={{ marginLeft: 10, marginRight: 10, }}>

                        <View style={{ height: 50, backgroundColor: 'rgba(255, 255, 255, 0.6)', marginTop: 10, borderRadius: 7, justifyContent: 'center', alignItems: 'center', flexDirection: 'row' }}>
                          
                        <TouchableOpacity onPress={() => this.tabClicks('notification')} style={{ width: '49%', height: 45, borderRadius: 7, alignItems: 'center', marginRight: 2, justifyContent: 'center'}}>
                            <Text> Notifications </Text>
                          </TouchableOpacity>

                          <TouchableOpacity onPress={() => this.tabClicks('visit')} style={{ width: '49%', height: 45, backgroundColor: '#fff',  marginRight: 2, borderRadius: 7}}>
                            <View style={{ width: '100%', height: 45, borderRadius: 7, alignItems: 'center', justifyContent: 'center'}}>
                              <Text> Visits </Text>
                            </View>
                          </TouchableOpacity>
                                                
                        </View>
                    </View>

                    <View style={{ flex: 1, borderTopWidth: 0, borderBottomWidth: 0, marginTop: 10, backgroundColor: '#fff', borderRadius: 7, borderBottomEndRadius: 0, borderBottomStartRadius: 0 }}>
                        <View style={{ flexDirection: 'row', alignItems: 'center', paddingLeft: 10, backgroundColor: 'rgba(192, 193, 194, 0.3)', height: 40, margin: 10, borderRadius: 50, flexDirection: 'row' }}>
                            <TextInput
                                style={{ width: '90%', fontSize: 16, color: '#000' }} 
                                placeholder='Search'
                                onChangeText={text => this.searchFunction(text)}
                                autoCorrect={false}
                                autoCapitalize = 'none'
                            // onChangeText={(query) => this.setState({ search: query })}
                            />
                            <Icon style={{ color: '#000'}} name={'search'} size={20} />
                        </View>

                        {this.renderFlatListVisits()}         
                    </View>

                    <TouchableOpacity  onPress={()=> Actions.FatherUserList1({callbackstate:this.tabClicks,pageMode1:this.state.pageMode})} style={{ position: 'absolute', marginLeft: width, marginTop: height }}>
                        <View style={{ width: 60, height: 60, justifyContent: 'center', alignItems: 'center', backgroundColor: '#0ea0cc', borderRadius: 50 }}>
                            <Text style={{ fontSize: 25, color: '#fff', fontWeight: 'bold'}}>+</Text>
                        </View>
                    </TouchableOpacity>

                  </View>
          );
        }
      }

    render() {
        return (
            <View style={{ flex: 1, backgroundColor: '#7BCFE8'}}>
              {this.renderItems()}
            </View>
        );
    }
}
