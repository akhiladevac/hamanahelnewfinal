import React, { Component } from 'react';
import { View, Text, FlatList, TouchableOpacity, Image, TextInput,BackHandler } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import AsyncStorage from '@react-native-community/async-storage';
import { Actions } from 'react-native-router-flux';
const DATA_BASE_URL = '@base_url';
const LOGIN_CREDENTIALS = '@login_credentials';

const DATA_LIST_MEMBERS = '@members_list';

export default class FamilyList extends Component {

    constructor() {
        super();
        this.getUpdatedUserslist = this.getUpdatedUserslist.bind(this)


        this.state = {
            loading: false,      
            data: [],
            temp: [],      
            error: null, 
            abd:[]   
          };

        this.arrayholder = [];
    }
getUpdatedUserslist(){
  this.getMembersList();
  this.getUpdatedUsers();


}
    componentDidMount() {
      console.log(Actions.currentScene)
        this.getMembersList();
         this.getUpdatedUsers();

        this.backHandler = BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);     

    }
    getUpdatedUsers(){
      this.getBaseUrl();
    }
    async getBaseUrl() {
      try {
          let baseUrl = await AsyncStorage.getItem(DATA_BASE_URL);
          this.setState({ baseUrl: baseUrl });
          var loginCredentials = await AsyncStorage.getItem(LOGIN_CREDENTIALS);
          var login_credentials = JSON.parse(loginCredentials);
          //this.setState({ baseUrl: baseUrl, fam_id: login_credentials.family_id})
          var fam_id = login_credentials.family_id;
        
        
          this.UdtUsersList(baseUrl,fam_id);
          
      } catch (error) {
          console.error('Something went Wrong in Get Section' + error);
      }
  }
  UdtUsersList(baseUrl,fam_id){
    var endpoint = 'list_of_members.php';
     var url = baseUrl + endpoint;
      // console.log(url);
      var data = new FormData();
      data.append('hashcode', '##church00@');
      data.append('Fam_Id',fam_id)
   
      fetch(url, {
      method: 'POST',
      headers: {
          Accept: 'application/json',
          'Content-Type': 'multipart/form-data',
      },
      body: data,
      })
    .then(response => response.json())
    .then((responseJson)=> {

      if (responseJson.status === 200) {
        this.setState({
          loading: false,
          abd: responseJson.response
         })

         this.chechVariations();
      }
      
    })
    .catch(error=>console.log(error)) //to catch the errors if any
    }

    async chechVariations() {
      try {
          const membersList = await AsyncStorage.getItem(DATA_LIST_MEMBERS);
          if (membersList !== null) {
            const abc = JSON.parse(membersList);
          
            this.setState({          
                data: abc,  
                temp: abc,        
                error: null,          
                loading: false,
                query: '',        
              });  
              this.arrayholder = abc; 
            } 

            if(JSON.stringify(this.state.abd) == JSON.stringify(this.state.data))
            { }else{
              this.updateAsyncValues(this.state.abd,DATA_LIST_MEMBERS);
            }
        
      } catch (error) {
          console.log('Something went Wrong Saving list');
      }
  }
  async updateAsyncValues(response, KEY) {
    await AsyncStorage.setItem(KEY, JSON.stringify(response));
    this.getMembersList();
  }
    componentWillUnmount() {
      this.backHandler.remove();
     }
     
     handleBackPress = () => {
      
       if (Actions.currentScene !== 'FamilyList') {
       } 
       else 
       {
           Actions.pop();

     
         return true;
       }
     }
    async getMembersList() {
        this.setState({ loading: true });
        try {
            const membersList = await AsyncStorage.getItem(DATA_LIST_MEMBERS);
            if (membersList !== null) {
              const abc = JSON.parse(membersList);
            this.setState({          
                data: abc,  
                temp: abc,        
                error: null,          
                loading: false,
                query: '',        
              });  
              this.arrayholder = abc; 
              //console.log(this.state.data)
            } 
        } catch (error) {
            console.log('Something went Wrong Saving list');
        }
    }

    searchFunction(query) {
      console.log(Actions.currentScene)

        if (query.length === 0) {
            this.setState({ data: this.state.temp, query: query });
        } else {
            this.setState({ query: query, });
            this.searchFilterFunction(this.state.query);
        }
        
    }

    searchFilterFunction = text => {    
        const newData = this.arrayholder.filter(item => {      
          const itemData = `${item.Fam_Owner.toUpperCase()}   
                            ${item.Fam_Name.toUpperCase()}`; 
          
           const textData = text.toUpperCase();
            
           return itemData.indexOf(textData) > -1;    
        });
        
        this.setState({ data: newData });  
      };

      renderProperties = ({ item }) => {
        return (
          <TouchableOpacity onPress={() => Actions.EditFamily({ fam_details: item ,parentCallback: this.getUpdatedUserslist})}>
            <View style={{ backgroundColor: '#fff', height: 70, marginTop: 10, marginLeft: 10, marginRight: 10, borderRadius: 10 }}>
              <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center' }}>
                  <Image 
                  style={{ width: 50, height: 50, marginLeft: 10 }}
                  source={require('../../img/avatar.png')}
                  />
                  <View>
                      <Text style={{ paddingLeft: 15, fontSize: 18, fontWeight: '500' }}>{ item.Fam_Owner }</Text>
                      <Text style={{ paddingLeft: 15, paddingTop: 8 }}>{ item.Fam_Name } House</Text>
                  </View>
                
              </View>
            </View>
          </TouchableOpacity>
        );
      };

    render() {
        return (
            <View style={{ borderTopWidth: 0, borderBottomWidth: 0 }}>
            <View style={{  alignItems: 'center', backgroundColor: '#7BCFE8' }}>
                  <Text style={{fontSize: 13, fontWeight: 'bold', color: '#000', paddingTop: 10}}>
                    Choose Family
                  </Text>
                </View>
                <View style={{ backgroundColor: '#7BCFE8', }}>
                
                <View style={{ flexDirection: 'row', alignItems: 'center', paddingLeft: 10, backgroundColor: '#fff', height: 40, margin: 20, marginTop: 15, borderRadius: 50, flexDirection: 'row' }}>
                    <TextInput
                      style={{ width: '90%', fontSize: 16, color: '#000' }} 
                      placeholder='Search'
                      onChangeText={text => this.searchFunction(text)}
                      autoCorrect={false}
                      // onChangeText={(query) => this.setState({ search: query })}
                    />
                    <Icon style={{ color: '#000'}} name={'search'} size={20} />
                </View>
                </View>
                <FlatList   
                    style={{ marginTop: 2 }}       
                    data={this.state.data}          
                    renderItem={this.renderProperties}          
                    keyExtractor={(item, index) => index.toString()}  
                    ItemSeparatorComponent={this.renderSeparator}                             
                />            
            </View>
        );
    }
}